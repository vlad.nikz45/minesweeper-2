import json
import os

from mvc.controller import MinesweeperController
from mvc.view import MinesweeperView
from packages.player import Player

path_to_index_last_save = "../saves/index_last_save.json"
if os.path.exists(path_to_index_last_save):
    with open(path_to_index_last_save) as json_file:
        data = json.load(json_file)
        index_last_save = int(data['index_last_save'])
else:
    index_last_save = 0

player = Player(index_last_save)

controller = MinesweeperController()
controller.set_player(player)

view = MinesweeperView(controller)
controller.set_view(view)
controller.open_main_menu()
