import json
import os.path
import random


class Player:
    """Игрок"""

    def __init__(self, index: int = 0, health_points: int = 5):
        """Инициализация при загрузке игры"""
        self.__number_levels = 9
        self.__number_items_in_shop = 5
        self.set_current_save_index(index)
        self.__set_path_to_saves("../saves")
        self.__set_file_name()
        if os.path.isfile(self.__save_name_file):
            self.__load_states_current_player()
        else:
            # Стандартные игровые статистики
            self.__default_health_points = health_points

            # Игровые характеристики
            self.init_or_restore_states_after_game_over()

    def set_current_save_index(self, index: int = 0):
        """Установка ныненего индекса сохранения"""
        self.__index = index
        self.save_index_last_save_in_file()

    def save_index_last_save_in_file(self):
        """Запись последнего сохранения (индекса) игрока в файл для продолжения при перезаходе в игру"""
        data = {"index_last_save": str(self.__index)}
        path_to_index_last_save = "../saves/index_last_save.json"
        with open(path_to_index_last_save, 'w') as outfile:
            json.dump(data, outfile)

    def __set_file_name(self):
        """Установка нынешнего имени файла сохранения"""
        self.__save_name_file = self.__path_to_saves + "/" + "save_player_" + str(self.__index) + ".json"

    def __set_path_to_saves(self, path: str):
        """Установка директории для файлов сохранения"""
        self.__path_to_saves = path

    def init_or_restore_states_after_game_over(self):
        """Установить значения по умолчаню после проигрыша в игре или при инициализации"""
        self.__max_health_points = self.__default_health_points
        self.__health_points = self.__max_health_points
        self.__number_coins = 0
        self.__inventory = [0, 0, 0, 0]
        self.__lvl_is_not_complete_list = [True for _ in range(self.__number_levels)]
        self.__shop_items = [random.randint(1, 5) for _ in range(self.__number_items_in_shop)]
        # self.__cost_items_shop = [random.randint(0, 2) + self.__shop_items[item] for item in
        #                           range(self.__number_items_in_shop)]
        self.__cost_items_shop = [random.randint(1, 5) for _ in
                                  range(self.__number_items_in_shop)]

        self.save_states_current_player()

    def __load_states_current_player(self):
        """Загрузка из файла сохранений игрока по индексу"""
        self.__set_file_name()
        if os.path.exists(self.__save_name_file):
            with open(self.__save_name_file) as json_file:
                data = json.load(json_file)
                for d in data['states']:
                    self.__default_health_points = int(d['default_health_points'])
                    self.__max_health_points = int(d['max_health_points'])
                    self.__health_points = int(d['health_points'])
                    self.__number_coins = int(d['number_coins'])
                    self.__index = int(d['index'])
                for d in data['inventory']:
                    self.__inventory = []
                    self.__inventory.append(int(d['slot_0']))
                    self.__inventory.append(int(d['slot_1']))
                    self.__inventory.append(int(d['slot_2']))
                    self.__inventory.append(int(d['slot_3']))
                for d in data['levels_not_complete']:
                    self.__lvl_is_not_complete_list = []
                    self.__lvl_is_not_complete_list.append(eval(d['level_0']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_1']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_2']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_3']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_4']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_5']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_6']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_7']))
                    self.__lvl_is_not_complete_list.append(eval(d['level_8']))
                for d in data['shop_items']:
                    self.__shop_items = []
                    self.__shop_items.append(int(d['slot_0']))
                    self.__shop_items.append(int(d['slot_1']))
                    self.__shop_items.append(int(d['slot_2']))
                    self.__shop_items.append(int(d['slot_3']))
                    self.__shop_items.append(int(d['slot_4']))
                for d in data['cost_items_shop']:
                    self.__cost_items_shop = []
                    self.__cost_items_shop.append(int(d['slot_0']))
                    self.__cost_items_shop.append(int(d['slot_1']))
                    self.__cost_items_shop.append(int(d['slot_2']))
                    self.__cost_items_shop.append(int(d['slot_3']))
                    self.__cost_items_shop.append(int(d['slot_4']))
        print('load: dhp, maxhp, hp, coins, invent', self.__default_health_points, self.__max_health_points,
              self.__health_points, self.__number_coins, self.__inventory, self.__lvl_is_not_complete_list,
              self.__shop_items, self.__cost_items_shop)

    def save_states_current_player(self):
        """Сохранение нынешних состояний игрока в файл по индексу"""
        self.__set_file_name()
        data = {'states': [], 'inventory': [], 'levels_not_complete': [], 'shop_items': [], 'cost_items_shop': []}
        data['states'].append({
            'default_health_points': str(self.__default_health_points),
            'max_health_points': str(self.__max_health_points),
            'health_points': str(self.__health_points),
            'number_coins': str(self.__number_coins),
            'index': str(self.__index)
        })
        data['inventory'].append({
            'slot_0': str(self.__inventory[0]),
            'slot_1': str(self.__inventory[1]),
            'slot_2': str(self.__inventory[2]),
            'slot_3': str(self.__inventory[3])
        })
        data['levels_not_complete'].append({
            'level_0': str(self.__lvl_is_not_complete_list[0]),
            'level_1': str(self.__lvl_is_not_complete_list[1]),
            'level_2': str(self.__lvl_is_not_complete_list[2]),
            'level_3': str(self.__lvl_is_not_complete_list[3]),
            'level_4': str(self.__lvl_is_not_complete_list[4]),
            'level_5': str(self.__lvl_is_not_complete_list[5]),
            'level_6': str(self.__lvl_is_not_complete_list[6]),
            'level_7': str(self.__lvl_is_not_complete_list[7]),
            'level_8': str(self.__lvl_is_not_complete_list[8])
        })
        data['shop_items'].append({
            'slot_0': str(self.__shop_items[0]),
            'slot_1': str(self.__shop_items[1]),
            'slot_2': str(self.__shop_items[2]),
            'slot_3': str(self.__shop_items[3]),
            'slot_4': str(self.__shop_items[4])
        })
        data['cost_items_shop'].append({
            'slot_0': str(self.__cost_items_shop[0]),
            'slot_1': str(self.__cost_items_shop[1]),
            'slot_2': str(self.__cost_items_shop[2]),
            'slot_3': str(self.__cost_items_shop[3]),
            'slot_4': str(self.__cost_items_shop[4])
        })

        with open(self.__save_name_file, 'w') as outfile:
            json.dump(data, outfile)
        print('save: dhp, maxhp, hp, coins, invent', self.__default_health_points, self.__max_health_points,
              self.__health_points, self.__number_coins, self.__inventory, self.__lvl_is_not_complete_list,
              self.__shop_items, self.__cost_items_shop)

    def delete_save_file(self):
        """Удаление файла сохранений по индексу игрока"""
        self.__set_file_name()
        if os.path.isfile(self.__save_name_file):
            os.remove(self.__save_name_file)
            print("Удалено")
        else:
            print("Файл не существует")

    @property
    def health_points(self):
        """Вовзрат значения очков здоровья"""
        return self.__health_points

    @property
    def max_health_points(self):
        """Вовзрат максимального значения очков здоровья"""
        return self.__max_health_points

    @max_health_points.setter
    def max_health_points(self, value):
        """Установка максимального значения очков здоровья"""
        self.__max_health_points = value
        self.save_states_current_player()

    @health_points.setter
    def health_points(self, value):
        """Установка значения очков здоровья"""
        self.__health_points = value
        self.save_states_current_player()

    @property
    def number_coins(self):
        """Возврат значения количества монет"""
        return self.__number_coins

    @number_coins.setter
    def number_coins(self, value):
        """Установка значения количества монет"""
        self.__number_coins = value
        self.save_states_current_player()

    @property
    def lvl_is_not_complete_list(self):
        """Возрат списка не пройдённых уровней"""
        return self.__lvl_is_not_complete_list

    @property
    def shop_items(self):
        """Возрат списка предметов в магазине"""
        return self.__shop_items

    @property
    def cost_items_shop(self):
        """Возрат списка цен предметов в магазине"""
        return self.__cost_items_shop

    @property
    def inventory(self):
        """Возврат инвентаря игрока"""
        return self.__inventory
