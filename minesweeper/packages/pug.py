from time import time as timer


class Pug:
    """Мопс анимирован"""

    def __init__(self, number_images: int):
        """Инициализация"""
        self.__images = [0 for _ in range(number_images + 1)]
        self.__counter = 0
        self.__index = 0
        self.__pug_speed = 5
        self.__pug_is_not_updating = True

    def update_image(self):
        """Обновление анимации. Контроллер"""
        long_update_in_sec = 0.05
        if self.__pug_is_not_updating:
            self.__pug_is_not_updating = False
            self.__update_image()
        elif timer() - self.__start_last_update_time >= long_update_in_sec:
            self.__pug_is_not_updating = True
            self.__update_image()

    def __update_image(self):
        """Обновление анимации"""
        self.__start_last_update_time = timer()
        # скорость обновления анимации монеты
        self.__counter += 1
        # переход на следующий спрайт и проверка за выход из списка изображений
        if self.__counter >= self.__pug_speed and self.__index < len(self.__images) - 1:
            self.__counter = 0
            # следующий img
            self.__index += 1
        # если анимация завершена, то экземпляр повторяется
        if self.__index >= len(self.__images) - 1:
            self.__index = 0

    @property
    def index(self):
        return self.__index
