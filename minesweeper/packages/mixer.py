import pygame
from time import time as timer
from packages.sounds import boom_sound, lose_sound, open_cell_sound, put_flag_sound, win_sound, pickup_sound,\
    menu_select


class Mixer:
    """Класс микшера"""

    def __init__(self):
        """Инициализация"""
        self.__global_sound_volume = 1
        self.__global_music_volume = 1
        self.__boom_sound = pygame.mixer.Sound(boom_sound)
        self.__current_music = None
        self.__boom_sound_is_not_playing = True
        self.__open_cell_sound_is_not_playing = True
        self.__menu_select_sound_is_not_playing = True
        self.__win_sound = pygame.mixer.Sound(win_sound)
        self.__lose_sound = pygame.mixer.Sound(lose_sound)
        self.__open_cell_sound = pygame.mixer.Sound(open_cell_sound)
        self.__put_flag_sound = pygame.mixer.Sound(put_flag_sound)
        self.__pickup_sound = pygame.mixer.Sound(pickup_sound)
        self.__menu_select = pygame.mixer.Sound(menu_select)

    def set_global_sound_volume(self, value):
        """Установление глобальной громкости звуков"""
        self.__global_sound_volume = value

    def set_music(self, music, volume=None):
        """Установление текущей музыки"""
        if self.__current_music != music:
            if volume is None:
                volume = self.__global_music_volume
            self.__current_music = music
            pygame.mixer.music.unload()
            pygame.mixer.music.load(music)
            pygame.mixer.music.set_volume(volume)
            pygame.mixer.music.play(-1)

    def set_global_music_volume(self, value):
        """Установление глобальной громкости музыки"""
        self.__global_music_volume = value

    def play_boom_sound(self, volume=None):
        """Проигрывание звука взрыва. Контроллер"""
        long_sound_in_sec = 0.18
        if self.__boom_sound_is_not_playing:
            self.__boom_sound_is_not_playing = False
            self.__play_boom_sound(volume)
        elif timer() - self.__start_last_boom_time >= long_sound_in_sec:
            self.__boom_sound_is_not_playing = True
            self.__play_boom_sound(volume)

    def __play_boom_sound(self, volume=None):
        """Проигрывание звука взрыва"""
        self.__start_last_boom_time = timer()
        if volume is None:
            volume = self.__global_sound_volume
        self.__boom_sound.set_volume(volume)
        self.__boom_sound.play()

    def play_menu_select_sound(self, volume=None):
        """Проигрывание звука нажатия в меню. Контроллер"""
        long_sound_in_sec = 0.18
        if self.__menu_select_sound_is_not_playing:
            self.__menu_select_sound_is_not_playing = False
            self.__play_menu_select_sound(volume)
        elif timer() - self.__start_last_menu_select_time >= long_sound_in_sec:
            self.__menu_select_sound_is_not_playing = True
            self.__play_menu_select_sound(volume)

    def __play_menu_select_sound(self, volume=None):
        """Проигрывание звука нажатия в меню"""
        self.__start_last_menu_select_time = timer()
        if volume is None:
            volume = self.__global_sound_volume
        self.__menu_select.set_volume(volume)
        self.__menu_select.play()

    def play_pickup_sound(self, volume=None):
        """Проигрывание звука подбора предмета"""
        if volume is None:
            volume = self.__global_sound_volume
        self.__pickup_sound.set_volume(volume)
        self.__pickup_sound.play()

    def play_win_sound(self, volume=None):
        """Проигрывание звука победы"""
        if volume is None:
            volume = self.__global_sound_volume
        self.__win_sound.set_volume(volume)
        self.__win_sound.play()

    def play_lose_sound(self, volume=None):
        """Проигрывание звука проигрыша"""
        if volume is None:
            volume = self.__global_sound_volume
        self.__lose_sound.set_volume(volume)
        self.__lose_sound.play()

    def play_open_cell_sound(self, volume=None):
        """Проигрывание звука открытия ячейки. Контроллер"""
        long_sound_in_sec = 0.18
        if self.__open_cell_sound_is_not_playing:
            self.__open_cell_sound_is_not_playing = False
            self.__play_open_cell_sound(volume)
        elif timer() - self.__start_last_boom_time >= long_sound_in_sec:
            self.__open_cell_sound_is_not_playing = True
            self.__play_open_cell_sound(volume)

    def __play_open_cell_sound(self, volume=None):
        """Проигрывание звука открытия ячейки"""
        self.__start_last_boom_time = timer()
        if volume is None:
            volume = self.__global_sound_volume
        self.__open_cell_sound.set_volume(volume)
        self.__open_cell_sound.play()

    def play_put_flag_sound(self, volume=None):
        """Проигрывание звука установки флага"""
        if volume is None:
            volume = self.__global_sound_volume
        self.__put_flag_sound.set_volume(volume)
        self.__put_flag_sound.play()


def main():
    """Запуск модуля"""
    pygame.init()
    mixer = Mixer()
    mixer.set_music(menu_music)

    W, H = 500, 300
    sc = pygame.display.set_mode((W, H))

    clock = pygame.time.Clock()
    FPS = 60

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    print('boom')
                    mixer.play_boom_sound()
        clock.tick(FPS)


if __name__ == "__main__":
    main()
