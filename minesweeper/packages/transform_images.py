import pygame

# Background
bg_start_screen_image = pygame.image.load("images/main_menu/main_menu_background.png")

# Shop
shop_bg = pygame.image.load("images/shop/shop.jpg")

# level 1
bg_lvl_image = pygame.image.load("images/levels/01/level_background.jpg")
mini_lvl1_image = pygame.image.load("images/all_levels_menu/level_01_disable.png")
mini_lvl1_image_enabled = pygame.image.load("images/all_levels_menu/level_01_enable.png")
big_lvl1_image = pygame.image.load("images/all_levels_menu/level_01_preview_02.png")
cell_close_image = pygame.image.load("images/levels/01/cell_close.png")
cell_open_image = pygame.image.load("images/levels/01/cell_open.png")
cell_close_flag_image = pygame.image.load("images/levels/01/cell_close_flag.png")
cell_mine_image = pygame.image.load("images/levels/01/cell_open_bomb.png")
cell_open_01_image = pygame.image.load("images/levels/01/cell_open_01.png")
cell_open_02_image = pygame.image.load("images/levels/01/cell_open_02.png")
cell_open_03_image = pygame.image.load("images/levels/01/cell_open_03.png")
cell_open_04_image = pygame.image.load("images/levels/01/cell_open_04.png")
cell_open_05_image = pygame.image.load("images/levels/01/cell_open_05.png")
cell_open_06_image = pygame.image.load("images/levels/01/cell_open_06.png")
cell_open_07_image = pygame.image.load("images/levels/01/cell_open_07.png")
cell_open_08_image = pygame.image.load("images/levels/01/cell_open_08.png")
# cell_loot_heart_image = pygame.image.load("images/interface_icons/HP.png")

# level 2
big_lvl2_image = pygame.image.load("images/all_levels_menu/level_02_preview_02.png")
mini_lvl2_image = pygame.image.load("images/all_levels_menu/level_02_disable.png")
mini_lvl2_image_enabled = pygame.image.load("images/all_levels_menu/level_02_enable.png")
bg_lvl_image_2 = pygame.image.load("images/levels/02/level_background.jpg")
cell_close_image_2 = pygame.image.load("images/levels/02/cell_02_closed.png")
cell_open_image_2 = pygame.image.load("images/levels/02/cell_02_open.png")
cell_close_flag_image_2 = pygame.image.load("images/levels/02/cell_02_flag.png")
cell_mine_image_2 = pygame.image.load("images/levels/02/cell_02_bomb.png")
cell_open_01_image_2 = pygame.image.load("images/levels/02/cell_numbers/01.png")
cell_open_02_image_2 = pygame.image.load("images/levels/02/cell_numbers/02.png")
cell_open_03_image_2 = pygame.image.load("images/levels/02/cell_numbers/03.png")
cell_open_04_image_2 = pygame.image.load("images/levels/02/cell_numbers/04.png")
cell_open_05_image_2 = pygame.image.load("images/levels/02/cell_numbers/05.png")
cell_open_06_image_2 = pygame.image.load("images/levels/02/cell_numbers/06.png")
cell_open_07_image_2 = pygame.image.load("images/levels/02/cell_numbers/07.png")
cell_open_08_image_2 = pygame.image.load("images/levels/02/cell_numbers/08.png")

# level 3
big_lvl3_image = pygame.image.load("images/all_levels_menu/level_03_preview_02.png")
mini_lvl3_image = pygame.image.load("images/all_levels_menu/level_03_disable.png")
mini_lvl3_image_enabled = pygame.image.load("images/all_levels_menu/level_03_enable.png")
bg_lvl_image_3 = pygame.image.load("images/levels/03/level_03.jpg")
cell_close_image_3 = pygame.image.load("images/levels/03/cell_03_closed.png")
cell_open_image_3 = pygame.image.load("images/levels/03/cell_03_open.png")
cell_close_flag_image_3 = pygame.image.load("images/levels/03/cell_03_flag.png")
cell_mine_image_3 = pygame.image.load("images/levels/03/cell_03_bomb.png")
cell_open_01_image_3 = pygame.image.load("images/levels/03/cell_numbers/01.png")
cell_open_02_image_3 = pygame.image.load("images/levels/03/cell_numbers/02.png")
cell_open_03_image_3 = pygame.image.load("images/levels/03/cell_numbers/03.png")
cell_open_04_image_3 = pygame.image.load("images/levels/03/cell_numbers/04.png")
cell_open_05_image_3 = pygame.image.load("images/levels/03/cell_numbers/05.png")
cell_open_06_image_3 = pygame.image.load("images/levels/03/cell_numbers/06.png")
cell_open_07_image_3 = pygame.image.load("images/levels/03/cell_numbers/07.png")
cell_open_08_image_3 = pygame.image.load("images/levels/03/cell_numbers/08.png")

# Interface
main_character_image = pygame.image.load("images/player_panel/main_character_animation/00.png")
main_character_1_image = pygame.image.load("images/player_panel/main_character_animation/09.png")
main_character_2_image = pygame.image.load("images/player_panel/main_character_animation/12.png")
main_character_3_image = pygame.image.load("images/player_panel/main_character_animation/16.png")
main_character_4_image = pygame.image.load("images/player_panel/main_character_animation/18.png")
main_character_5_image = pygame.image.load("images/player_panel/main_character_animation/20.png")
main_character_6_image = pygame.image.load("images/player_panel/main_character_animation/21.png")
hp_icon_image = pygame.image.load("images/player_panel/heart_panel.png")
cell_icon_image = pygame.image.load("images/interface_icons/inventory_cell.png")
lose_image = pygame.image.load("images/you_died_banner.png")
win_image = pygame.image.load("images/you_win_banner.png")
restart_button_image = pygame.image.load("images/interface_icons/you_died_button.png")
coin_icon_image = pygame.image.load("images/player_panel/coin_panel.png")
go_to_menu_image = pygame.image.load("images/go_to_menu_icon.png")
# down_panel_image = pygame.image.load("images/player_panel/background.png")


# Main menu
game_article = pygame.image.load("images/main_menu/pug_traveler.png")
# Lvl menu
lvl_ball_image = pygame.image.load("images/all_levels_menu/ball.png")
lvl_menu_bg_image = pygame.image.load("images/all_levels_menu/all_levels_menu.jpg")
bomb_count_image = pygame.image.load("images/all_levels_menu/bomb_count.png")
bomb_damage_image = pygame.image.load("images/all_levels_menu/bomb_damage.png")
cells_count_image = pygame.image.load("images/all_levels_menu/cell_count.png")
# Coin
# coin_0_image = pygame.image.load("images/coin_sprites/0.png")
# coin_1_image = pygame.image.load("images/coin_sprites/1.png")
# coin_2_image = pygame.image.load("images/coin_sprites/2.png")
# coin_3_image = pygame.image.load("images/coin_sprites/3.png")
# coin_4_image = pygame.image.load("images/coin_sprites/4.png")
# coin_5_image = pygame.image.load("images/coin_sprites/5.png")
# coin_6_image = pygame.image.load("images/coin_sprites/6.png")
# coin_7_image = pygame.image.load("images/coin_sprites/7.png")

coin_01_image = pygame.image.load("images/coin_sprites/dogecoin_sprites/field/01.png")
coin_02_image = pygame.image.load("images/coin_sprites/dogecoin_sprites/field/02.png")
coin_03_image = pygame.image.load("images/coin_sprites/dogecoin_sprites/field/03.png")
coin_04_image = pygame.image.load("images/coin_sprites/dogecoin_sprites/field/04.png")
coin_05_image = pygame.image.load("images/coin_sprites/dogecoin_sprites/field/05.png")
coin_06_image = pygame.image.load("images/coin_sprites/dogecoin_sprites/field/06.png")

# Heart
heart_01_image = pygame.image.load("images/heart_sprites/01.png")
heart_02_image = pygame.image.load("images/heart_sprites/02.png")
heart_03_image = pygame.image.load("images/heart_sprites/03.png")
heart_04_image = pygame.image.load("images/heart_sprites/04.png")
heart_05_image = pygame.image.load("images/heart_sprites/05.png")
heart_06_image = pygame.image.load("images/heart_sprites/06.png")
heart_07_image = pygame.image.load("images/heart_sprites/07.png")
heart_08_image = pygame.image.load("images/heart_sprites/08.png")
heart_09_image = pygame.image.load("images/heart_sprites/09.png")
heart_10_image = pygame.image.load("images/heart_sprites/10.png")
heart_11_image = pygame.image.load("images/heart_sprites/11.png")
heart_12_image = pygame.image.load("images/heart_sprites/12.png")
heart_13_image = pygame.image.load("images/heart_sprites/13.png")

# Explosion
explosion_00_image = pygame.image.load("images/explosion_sprites/explosion_0.png")
explosion_01_image = pygame.image.load("images/explosion_sprites/explosion_1.png")
explosion_02_image = pygame.image.load("images/explosion_sprites/explosion_2.png")
explosion_03_image = pygame.image.load("images/explosion_sprites/explosion_3.png")
explosion_04_image = pygame.image.load("images/explosion_sprites/explosion_4.png")
explosion_05_image = pygame.image.load("images/explosion_sprites/explosion_5.png")
explosion_06_image = pygame.image.load("images/explosion_sprites/explosion_6.png")
explosion_07_image = pygame.image.load("images/explosion_sprites/explosion_7.png")
explosion_08_image = pygame.image.load("images/explosion_sprites/explosion_8.png")
explosion_09_image = pygame.image.load("images/explosion_sprites/explosion_9.png")
explosion_10_image = pygame.image.load("images/explosion_sprites/explosion_10.png")
explosion_11_image = pygame.image.load("images/explosion_sprites/explosion_11.png")
explosion_12_image = pygame.image.load("images/explosion_sprites/explosion_12.png")

# CollectableItems
orange_image = pygame.image.load("images/collectable_items/orange.png")
banana_image = pygame.image.load("images/collectable_items/banana.png")
strawberry_image = pygame.image.load("images/collectable_items/cherry.png")
apple_image = pygame.image.load("images/collectable_items/kiwi.png")
cell_opening_staff = pygame.image.load("images/collectable_items/brush.png")


class TransformImages:
    """Класс для трансформации картинок в нужный размер"""

    def __init__(self, screen_width, screen_height):
        """Инициализация"""
        self.__screen_width = screen_width
        self.__screen_height = screen_height

        # load lvl_menu
        self.__lvl_ball_image = lvl_ball_image
        self.__lvl_menu_bg_image = lvl_menu_bg_image
        self.__bomb_count_image = bomb_count_image
        self.__cells_count_image = cells_count_image
        self.__bomb_damage_image = bomb_damage_image

        # load bg
        self.__bg_start_screen_image = bg_start_screen_image
        # Магазин
        self.__shop_bg = shop_bg
        # Миниатюры
        self.__lvl1_image = mini_lvl1_image
        self.__lvl1_image_enabled = mini_lvl1_image_enabled
        self.__lvl2_image = mini_lvl2_image
        self.__lvl2_image_enabled = mini_lvl2_image_enabled
        self.__lvl3_image = mini_lvl3_image
        self.__lvl3_image_enabled = mini_lvl3_image_enabled

        # Большие миниатюры
        self.__lvl1_image_big = big_lvl1_image
        self.__lvl2_image_big = big_lvl2_image
        self.__lvl3_image_big = big_lvl3_image

        # load level 1
        self.__bg_lvl_image = bg_lvl_image
        self.__cell_close_image = cell_close_image
        self.__cell_open_image = cell_open_image
        self.__cell_close_flag_image = cell_close_flag_image
        self.__cell_mine_image = cell_mine_image
        self.__cell_open_01_image = cell_open_01_image
        self.__cell_open_02_image = cell_open_02_image
        self.__cell_open_03_image = cell_open_03_image
        self.__cell_open_04_image = cell_open_04_image
        self.__cell_open_05_image = cell_open_05_image
        self.__cell_open_06_image = cell_open_06_image
        self.__cell_open_07_image = cell_open_07_image
        self.__cell_open_08_image = cell_open_08_image

        # load level 2
        self.__bg_lvl_image_2 = bg_lvl_image_2
        self.__cell_close_image_2 = cell_close_image_2
        self.__cell_open_image_2 = cell_open_image_2
        self.__cell_close_flag_image_2 = cell_close_flag_image_2
        self.__cell_mine_image_2 = cell_mine_image_2
        self.__cell_open_01_image_2 = cell_open_01_image_2
        self.__cell_open_02_image_2 = cell_open_02_image_2
        self.__cell_open_03_image_2 = cell_open_03_image_2
        self.__cell_open_04_image_2 = cell_open_04_image_2
        self.__cell_open_05_image_2 = cell_open_05_image_2
        self.__cell_open_06_image_2 = cell_open_06_image_2
        self.__cell_open_07_image_2 = cell_open_07_image_2
        self.__cell_open_08_image_2 = cell_open_08_image_2

        # load level 3
        self.__bg_lvl_image_3 = bg_lvl_image_3
        self.__cell_close_image_3 = cell_close_image_3
        self.__cell_open_image_3 = cell_open_image_3
        self.__cell_close_flag_image_3 = cell_close_flag_image_3
        self.__cell_mine_image_3 = cell_mine_image_3
        self.__cell_open_01_image_3 = cell_open_01_image_3
        self.__cell_open_02_image_3 = cell_open_02_image_3
        self.__cell_open_03_image_3 = cell_open_03_image_3
        self.__cell_open_04_image_3 = cell_open_04_image_3
        self.__cell_open_05_image_3 = cell_open_05_image_3
        self.__cell_open_06_image_3 = cell_open_06_image_3
        self.__cell_open_07_image_3 = cell_open_07_image_3
        self.__cell_open_08_image_3 = cell_open_08_image_3

        # lists lvl images
        self.__bg_lvl_list = [self.__bg_lvl_image, self.__bg_lvl_image, self.__bg_lvl_image,
                              self.__bg_lvl_image_2, self.__bg_lvl_image_2, self.__bg_lvl_image_2,
                              self.__bg_lvl_image_3, self.__bg_lvl_image_3, self.__bg_lvl_image_3]
        self.__cell_close_list = [self.__cell_close_image, self.__cell_close_image, self.__cell_close_image,
                                  self.__cell_close_image_2, self.__cell_close_image_2, self.__cell_close_image_2,
                                  self.__cell_close_image_3, self.__cell_close_image_3, self.__cell_close_image_3]
        self.__cell_open_list = [self.__cell_open_image, self.__cell_open_image, self.__cell_open_image,
                                 self.__cell_open_image_2, self.__cell_open_image_2, self.__cell_open_image_2,
                                 self.__cell_open_image_3, self.__cell_open_image_3, self.__cell_open_image_3]
        self.__cell_close_flag_list = [self.__cell_close_flag_image, self.__cell_close_flag_image,
                                       self.__cell_close_flag_image, self.__cell_close_flag_image_2,
                                       self.__cell_close_flag_image_2, self.__cell_close_flag_image_2,
                                       self.__cell_close_flag_image_3, self.__cell_close_flag_image_3,
                                       self.__cell_close_flag_image_3]
        self.__cell_mine_list = [self.__cell_mine_image, self.__cell_mine_image, self.__cell_mine_image,
                                 self.__cell_mine_image_2, self.__cell_mine_image_2, self.__cell_mine_image_2,
                                 self.__cell_mine_image_3, self.__cell_mine_image_3, self.__cell_mine_image_3]
        self.__cell_open_01_list = [self.__cell_open_01_image, self.__cell_open_01_image, self.__cell_open_01_image,
                                    self.__cell_open_01_image_2, self.__cell_open_01_image_2,
                                    self.__cell_open_01_image_2,
                                    self.__cell_open_01_image_3, self.__cell_open_01_image_3,
                                    self.__cell_open_01_image_3]
        self.__cell_open_02_list = [self.__cell_open_02_image, self.__cell_open_02_image, self.__cell_open_02_image,
                                    self.__cell_open_02_image_2, self.__cell_open_02_image_2,
                                    self.__cell_open_02_image_2,
                                    self.__cell_open_02_image_3, self.__cell_open_02_image_3,
                                    self.__cell_open_02_image_3]
        self.__cell_open_03_list = [self.__cell_open_03_image, self.__cell_open_03_image, self.__cell_open_03_image,
                                    self.__cell_open_03_image_2, self.__cell_open_03_image_2,
                                    self.__cell_open_03_image_2,
                                    self.__cell_open_03_image_3, self.__cell_open_03_image_3,
                                    self.__cell_open_03_image_3]
        self.__cell_open_04_list = [self.__cell_open_04_image, self.__cell_open_04_image, self.__cell_open_04_image,
                                    self.__cell_open_04_image_2, self.__cell_open_04_image_2,
                                    self.__cell_open_04_image_2,
                                    self.__cell_open_04_image_3, self.__cell_open_04_image_3,
                                    self.__cell_open_04_image_3]
        self.__cell_open_05_list = [self.__cell_open_05_image, self.__cell_open_05_image, self.__cell_open_05_image,
                                    self.__cell_open_05_image_2, self.__cell_open_05_image_2,
                                    self.__cell_open_05_image_2,
                                    self.__cell_open_05_image_3, self.__cell_open_05_image_3,
                                    self.__cell_open_05_image_3]
        self.__cell_open_06_list = [self.__cell_open_06_image, self.__cell_open_06_image, self.__cell_open_06_image,
                                    self.__cell_open_06_image_2, self.__cell_open_06_image_2,
                                    self.__cell_open_06_image_2,
                                    self.__cell_open_06_image_3, self.__cell_open_06_image_3,
                                    self.__cell_open_06_image_3]
        self.__cell_open_07_list = [self.__cell_open_07_image, self.__cell_open_07_image, self.__cell_open_07_image,
                                    self.__cell_open_07_image_2, self.__cell_open_07_image_2,
                                    self.__cell_open_07_image_2,
                                    self.__cell_open_07_image_3, self.__cell_open_07_image_3,
                                    self.__cell_open_07_image_3]
        self.__cell_open_08_list = [self.__cell_open_08_image, self.__cell_open_08_image, self.__cell_open_08_image,
                                    self.__cell_open_08_image_2, self.__cell_open_08_image_2,
                                    self.__cell_open_08_image_2,
                                    self.__cell_open_08_image_3, self.__cell_open_08_image_3,
                                    self.__cell_open_08_image_3]

        # self.cell_loot_heart_image = cell_loot_heart_image

        # load interface icons
        self.__main_character_image = main_character_image
        self.__main_character_1_image = main_character_1_image
        self.__main_character_2_image = main_character_2_image
        self.__main_character_3_image = main_character_3_image
        self.__main_character_4_image = main_character_4_image
        self.__main_character_5_image = main_character_5_image
        self.__main_character_6_image = main_character_6_image
        self.__hp_icon_image = hp_icon_image
        self.__coin_icon_image = coin_icon_image
        self.__restart_button_image = restart_button_image
        # self.__down_panel_image = down_panel_image
        self.__game_article = game_article
        self.__cell_icon_image = cell_icon_image
        self.__lose_image = lose_image
        self.__win_image = win_image
        self.__go_to_menu_image = go_to_menu_image

        # load coin
        # self.coin_0_image = coin_0_image
        self.__coin_01_image = coin_01_image
        self.__coin_02_image = coin_02_image
        self.__coin_03_image = coin_03_image
        self.__coin_04_image = coin_04_image
        self.__coin_05_image = coin_05_image
        self.__coin_06_image = coin_06_image
        # self.coin_7_image = coin_7_image

        # load heart
        self.__heart_01_image = heart_01_image
        self.__heart_02_image = heart_02_image
        self.__heart_03_image = heart_03_image
        self.__heart_04_image = heart_04_image
        self.__heart_05_image = heart_05_image
        self.__heart_06_image = heart_06_image
        self.__heart_07_image = heart_07_image
        self.__heart_08_image = heart_08_image
        self.__heart_09_image = heart_09_image
        self.__heart_10_image = heart_10_image
        self.__heart_11_image = heart_11_image
        self.__heart_12_image = heart_12_image
        self.__heart_13_image = heart_13_image

        # load explosion
        self.__explosion_00_image = explosion_00_image
        self.__explosion_01_image = explosion_01_image
        self.__explosion_02_image = explosion_02_image
        self.__explosion_03_image = explosion_03_image
        self.__explosion_04_image = explosion_04_image
        self.__explosion_05_image = explosion_05_image
        self.__explosion_06_image = explosion_06_image
        self.__explosion_07_image = explosion_07_image
        self.__explosion_08_image = explosion_08_image
        self.__explosion_09_image = explosion_09_image
        self.__explosion_10_image = explosion_10_image
        self.__explosion_11_image = explosion_11_image
        self.__explosion_12_image = explosion_12_image

        # load collectable_items
        self.__orange_image = orange_image
        self.__banana_image = banana_image
        self.__strawberry_image = strawberry_image
        self.__apple_image = apple_image
        self.__cell_opening_staff = cell_opening_staff

        # transform
        self.__transform_main_character_image = pygame.transform.scale(self.__main_character_image, (
            self.__screen_height * 1 / 7, self.__screen_height * 1 / 7))
        self.__transform_main_character_1_image = pygame.transform.scale(self.__main_character_1_image, (
            self.__screen_height * 1 / 7, self.__screen_height * 1 / 7))
        self.__transform_main_character_2_image = pygame.transform.scale(self.__main_character_2_image, (
            self.__screen_height * 1 / 7, self.__screen_height * 1 / 7))
        self.__transform_main_character_3_image = pygame.transform.scale(self.__main_character_3_image, (
            self.__screen_height * 1 / 7, self.__screen_height * 1 / 7))
        self.__transform_main_character_4_image = pygame.transform.scale(self.__main_character_4_image, (
            self.__screen_height * 1 / 7, self.__screen_height * 1 / 7))
        self.__transform_main_character_5_image = pygame.transform.scale(self.__main_character_5_image, (
            self.__screen_height * 1 / 7, self.__screen_height * 1 / 7))
        self.__transform_main_character_6_image = pygame.transform.scale(self.__main_character_6_image, (
            self.__screen_height * 1 / 7, self.__screen_height * 1 / 7))
        self.__transform_bg_start_screen_image = pygame.transform.scale(self.__bg_start_screen_image,
                                                                        (self.__screen_width, self.__screen_height))
        self.__lvl_menu_bg_image = pygame.transform.scale(self.__lvl_menu_bg_image,
                                                          (self.__screen_width, self.__screen_height))
        self.__shop_bg = pygame.transform.scale(self.__shop_bg,
                                                (self.__screen_width, self.__screen_height))
        self.calc_bg_lvl_image()
        self.calc_bg_lvl_image_big()
        self.calc_interface_icons()
        self.randomize_images()

    def calc_transformation_background_level(self, index_level: int = 0):
        """Рассчитать трансформацию фона уровня"""
        self.__transform_bg_lvl_image = pygame.transform.scale(self.__bg_lvl_list[index_level],
                                                               (self.__screen_width, self.__screen_height))

    def calc_interface_icons(self):
        """Миниатюры подписей возле большой картинки миниатюры, прочие картинки"""
        self.__hp_icon_image = pygame.transform.scale(self.__hp_icon_image,
                                                      (self.__screen_width / 30, self.__screen_width / 30))
        self.__coin_icon_image = pygame.transform.scale(self.__coin_icon_image,
                                                        (self.__screen_width / 30, self.__screen_width / 30))
        # self.__down_panel_image = pygame.transform.scale(self.__down_panel_image,
        #                                                  (self.__screen_width, self.__screen_height / 6))
        self.__game_article = pygame.transform.scale(self.__game_article,
                                                     (self.__screen_width / 1.5, self.__screen_height / 5))
        self.__lvl_ball_image = pygame.transform.scale(self.__lvl_ball_image,
                                                       (self.__screen_width / 30, self.__screen_width / 30))

        self.__bomb_counter_image = pygame.transform.scale(self.__bomb_count_image,
                                                           (self.__screen_width / 10, self.__screen_width / 22))
        self.__bomb_damage_image = pygame.transform.scale(self.__bomb_damage_image,
                                                          (self.__screen_width / 10, self.__screen_width / 22))
        self.__cells_count_image = pygame.transform.scale(self.__cells_count_image,
                                                          (self.__screen_width / 10, self.__screen_width / 22))

        self.__transform_cell_icon_image = pygame.transform.scale(self.__cell_icon_image,
                                                                  (self.__screen_width / 20, self.__screen_width / 20))

        self.__transform_orange_image = pygame.transform.scale(self.__orange_image,
                                                               (self.__screen_width / 30, self.__screen_width / 30))
        self.__transform_banana_image = pygame.transform.scale(self.__banana_image,
                                                               (self.__screen_width / 30, self.__screen_width / 30))
        self.__transform_strawberry_image = pygame.transform.scale(self.__strawberry_image,
                                                                   (self.__screen_width / 30, self.__screen_width / 30))
        self.__transform_apple_image = pygame.transform.scale(self.__apple_image,
                                                              (self.__screen_width / 30, self.__screen_width / 30))
        self.__transform_cell_opening_staff = pygame.transform.scale(self.__cell_opening_staff,
                                                                     (self.__screen_width / 30,
                                                                      self.__screen_width / 30))
        self.__lose_image = pygame.transform.scale(self.__lose_image,
                                                   (self.__screen_width / 2, self.__screen_width / 3))
        self.__win_image = pygame.transform.scale(self.__win_image,
                                                  (self.__screen_width / 2, self.__screen_width / 3))
        self.__go_to_menu_image = pygame.transform.scale(self.__go_to_menu_image,
                                                         (self.__screen_width / 18, self.__screen_width / 20))
        self.__transform_restart_button_image = pygame.transform.scale(self.__restart_button_image,
                                                                       (self.__screen_width / 7,
                                                                        self.__screen_width / 7))

    def calc_bg_lvl_image(self):
        """Миниатюры картинок уровней"""
        self.__lvl1_image = pygame.transform.scale(self.__lvl1_image,
                                                   (self.__screen_width / 12, self.__screen_width / 12))
        self.__lvl1_image_enabled = pygame.transform.scale(self.__lvl1_image_enabled,
                                                           (self.__screen_width / 12, self.__screen_width / 12))
        self.__lvl2_image = pygame.transform.scale(self.__lvl2_image,
                                                   (self.__screen_width / 12, self.__screen_width / 12))
        self.__lvl2_image_enabled = pygame.transform.scale(self.__lvl2_image_enabled,
                                                           (self.__screen_width / 12, self.__screen_width / 12))
        self.__lvl3_image = pygame.transform.scale(self.__lvl3_image,
                                                   (self.__screen_width / 12, self.__screen_width / 12))
        self.__lvl3_image_enabled = pygame.transform.scale(self.__lvl3_image_enabled,
                                                           (self.__screen_width / 12, self.__screen_width / 12))

    @property
    def shop_bg(self):
        return self.__shop_bg

    @property
    def go_to_menu_image(self):
        return self.__go_to_menu_image

    @property
    def lose_image(self):
        return self.__lose_image

    @property
    def win_image(self):
        return self.__win_image

    @property
    def bomb_counter(self):
        return self.__bomb_counter_image

    @property
    def bomb_damage(self):
        return self.__bomb_damage_image

    @property
    def cells_count(self):
        return self.__cells_count_image

    @property
    def lvl_menu_bg(self):
        return self.__lvl_menu_bg_image

    @property
    def game_article(self):
        return self.__game_article

    @property
    def lvl_ball_image(self):
        return self.__lvl_ball_image

    # @property
    # def down_panel_image(self):
    #     return self.__down_panel_image

    @property
    def lvl1_image(self):
        """Возврат self.__lvl1_image"""
        return self.__lvl1_image

    @property
    def lvl1_image_enabled(self):
        return self.__lvl1_image_enabled

    @property
    def lvl2_image(self):
        """Возврат self.__lvl1_image"""
        return self.__lvl2_image

    @property
    def lvl2_image_enabled(self):
        return self.__lvl2_image_enabled

    @property
    def lvl3_image(self):
        """Возврат self.__lvl1_image"""
        return self.__lvl3_image

    @property
    def lvl3_image_enabled(self):
        return self.__lvl3_image_enabled

    @property
    def lvl1_image_big(self):
        """Возврат self.__lvl1_image_big"""
        return self.__lvl1_image_big

    @property
    def lvl2_image_big(self):
        """Возврат self.__lvl2_image_big"""
        return self.__lvl2_image_big

    @property
    def lvl3_image_big(self):
        """Возврат self.__lvl2_image_big"""
        return self.__lvl3_image_big

    @property
    def hp_icon_image(self):
        """Возврат self.__hp_icon_image"""
        return self.__hp_icon_image

    @property
    def coin_icon_image(self):
        """Возврат self.__coin_icon_image"""
        return self.__coin_icon_image

    @property
    def bomb_counter_image(self):
        """Возврат self.__bomb_counter_image"""
        return self.__bomb_counter_image

    def calc_bg_lvl_image_big(self):
        """Большие миниатюры картинок уровней"""
        self.__lvl1_image_big = pygame.transform.scale(self.__lvl1_image_big,
                                                       (self.__screen_width / 4, self.__screen_width / 4))
        self.__lvl2_image_big = pygame.transform.scale(self.__lvl2_image_big,
                                                       (self.__screen_width / 4, self.__screen_width / 4))
        self.__lvl3_image_big = pygame.transform.scale(self.__lvl3_image_big,
                                                       (self.__screen_width / 4, self.__screen_width / 4))

    def calculate_transformation_cells(self, width: int, index_level: int = 0, pd: int = 5):
        """Рассчитать трансформацию для каждой разновидности картинки для ячейки. Состояние ячейки"""
        self.__transform_cell_close_image = pygame.transform.scale(self.__cell_close_list[index_level],
                                                                   (width - pd, width - pd))
        self.__transform_cell_open_image = pygame.transform.scale(self.__cell_open_list[index_level],
                                                                  (width - pd, width - pd))
        self.__transform_cell_close_flag_image = pygame.transform.scale(self.__cell_close_flag_list[index_level],
                                                                        (width - pd, width - pd))
        self.__transform_cell_mine_image = pygame.transform.scale(self.__cell_mine_list[index_level],
                                                                  (width - pd, width - pd))
        self.__transform_cell_open_01_image = pygame.transform.scale(self.__cell_open_01_list[index_level],
                                                                     (width - pd, width - pd))
        self.__transform_cell_open_02_image = pygame.transform.scale(self.__cell_open_02_list[index_level],
                                                                     (width - pd, width - pd))
        self.__transform_cell_open_03_image = pygame.transform.scale(self.__cell_open_03_list[index_level],
                                                                     (width - pd, width - pd))
        self.__transform_cell_open_04_image = pygame.transform.scale(self.__cell_open_04_list[index_level],
                                                                     (width - pd, width - pd))
        self.__transform_cell_open_05_image = pygame.transform.scale(self.__cell_open_05_list[index_level],
                                                                     (width - pd, width - pd))
        self.__transform_cell_open_06_image = pygame.transform.scale(self.__cell_open_06_list[index_level],
                                                                     (width - pd, width - pd))
        self.__transform_cell_open_07_image = pygame.transform.scale(self.__cell_open_07_list[index_level],
                                                                     (width - pd, width - pd))
        self.__transform_cell_open_08_image = pygame.transform.scale(self.__cell_open_08_list[index_level],
                                                                     (width - pd, width - pd))

        self.calculate_transformation_loot(width * 0.5)
        self.calculate_transformation_explosion(width)

    def calculate_transformation_loot(self, width: float, pd: int = 3):
        """Рассчитать трансформацию для каждой разновидности картинки для ячейки. Лут"""
        # self.__transform_cell_loot_heart_image = pygame.transform.scale(self.cell_loot_heart_image,
        #                                                                 (width - pd, width - pd))
        # self.__transform_cell_loot_coin_0_image = pygame.transform.scale(self.coin_00_image, (width - pd, width - pd))
        self.__transform_cell_loot_coin_01_image = pygame.transform.scale(self.__coin_01_image,
                                                                          (width - pd, width - pd))
        self.__transform_cell_loot_coin_02_image = pygame.transform.scale(self.__coin_02_image,
                                                                          (width - pd, width - pd))
        self.__transform_cell_loot_coin_03_image = pygame.transform.scale(self.__coin_03_image,
                                                                          (width - pd, width - pd))
        self.__transform_cell_loot_coin_04_image = pygame.transform.scale(self.__coin_04_image,
                                                                          (width - pd, width - pd))
        self.__transform_cell_loot_coin_05_image = pygame.transform.scale(self.__coin_05_image,
                                                                          (width - pd, width - pd))
        self.__transform_cell_loot_coin_06_image = pygame.transform.scale(self.__coin_06_image,
                                                                          (width - pd, width - pd))
        # self.__transform_cell_loot_coin_7_image = pygame.transform.scale(self.coin_07_image, (width - pd, width - pd))

        self.__transform_cell_loot_heart_01_image = pygame.transform.scale(self.__heart_01_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_02_image = pygame.transform.scale(self.__heart_02_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_03_image = pygame.transform.scale(self.__heart_03_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_04_image = pygame.transform.scale(self.__heart_04_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_05_image = pygame.transform.scale(self.__heart_05_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_06_image = pygame.transform.scale(self.__heart_06_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_07_image = pygame.transform.scale(self.__heart_07_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_08_image = pygame.transform.scale(self.__heart_08_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_09_image = pygame.transform.scale(self.__heart_09_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_10_image = pygame.transform.scale(self.__heart_10_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_11_image = pygame.transform.scale(self.__heart_11_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_12_image = pygame.transform.scale(self.__heart_12_image,
                                                                           (width - pd, width - pd))
        self.__transform_cell_loot_heart_13_image = pygame.transform.scale(self.__heart_13_image,
                                                                           (width - pd, width - pd))

        self.__transform_cell_loot_orange_image = pygame.transform.scale(self.__orange_image,
                                                                         (width - pd, width - pd))
        self.__transform_cell_loot_banana_image = pygame.transform.scale(self.__banana_image,
                                                                         (width - pd, width - pd))
        self.__transform_cell_loot_strawberry_image = pygame.transform.scale(self.__strawberry_image,
                                                                             (width - pd, width - pd))
        self.__transform_cell_loot_apple_image = pygame.transform.scale(self.__apple_image,
                                                                        (width - pd, width - pd))
        self.__transform_cell_loot_cell_opening_staff = pygame.transform.scale(self.__cell_opening_staff,
                                                                               (width - pd, width - pd))

    def calculate_transformation_explosion(self, width: int):
        """Рассчитать трансформацию для взрыва при открытии бомбы"""
        self.__transform_explosion_00_image = pygame.transform.scale(self.__explosion_00_image, (width * 3, width * 3))
        self.__transform_explosion_01_image = pygame.transform.scale(self.__explosion_01_image, (width * 3, width * 3))
        self.__transform_explosion_02_image = pygame.transform.scale(self.__explosion_02_image, (width * 3, width * 3))
        self.__transform_explosion_03_image = pygame.transform.scale(self.__explosion_03_image, (width * 3, width * 3))
        self.__transform_explosion_04_image = pygame.transform.scale(self.__explosion_04_image, (width * 3, width * 3))
        self.__transform_explosion_05_image = pygame.transform.scale(self.__explosion_05_image, (width * 3, width * 3))
        self.__transform_explosion_06_image = pygame.transform.scale(self.__explosion_06_image, (width * 3, width * 3))
        self.__transform_explosion_07_image = pygame.transform.scale(self.__explosion_07_image, (width * 3, width * 3))
        self.__transform_explosion_08_image = pygame.transform.scale(self.__explosion_08_image, (width * 3, width * 3))
        self.__transform_explosion_09_image = pygame.transform.scale(self.__explosion_09_image, (width * 3, width * 3))
        self.__transform_explosion_10_image = pygame.transform.scale(self.__explosion_10_image, (width * 3, width * 3))
        self.__transform_explosion_11_image = pygame.transform.scale(self.__explosion_11_image, (width * 3, width * 3))
        self.__transform_explosion_12_image = pygame.transform.scale(self.__explosion_12_image, (width * 3, width * 3))

    def calculate_focus_close_cell(self, width: int, index_level: int = 0):
        """Рассчитать трансформацию для наведённой закрытой ячейки"""
        self.__transform_cell_close_image = pygame.transform.scale(self.__cell_close_list[index_level],
                                                                   (width - 2.5, width - 2.5))

    def randomize_images(self, path: str = 'snowflakes/snowflake', range_number: int = 4):
        """Создание списка снежинок с разными спрайтами и размером"""
        self.__snowflakes_images_list = []
        for i in range(1, range_number):
            image_filename = "images/" + path + "_" + str(i) + ".png"
            image = pygame.image.load(image_filename)
            for size in range(1, 6):
                transform_image = pygame.transform.scale(image, (size * self.__screen_width / 500,
                                                                 size * self.__screen_width / 500))
                self.__snowflakes_images_list.append(transform_image)

    @property
    def snowflakes_images_list(self):
        """Возврат трансформированного списка снежинок"""
        return self.__snowflakes_images_list

    @property
    def transform_bg_start_screen_image(self):
        """Возврат трансформированной картинки стартового экрана"""
        return self.__transform_bg_start_screen_image

    @property
    def transform_bg_lvl_image(self):
        """Возврат трансформированной картинки в уровне"""
        return self.__transform_bg_lvl_image

    @property
    def transform_cell_close_image(self):
        """Возврат трансформированной закрытой ячейки"""
        return self.__transform_cell_close_image

    @property
    def transform_cell_open_image(self):
        """Возврат трансформированной открытой ячейки"""
        return self.__transform_cell_open_image

    @property
    def transform_cell_close_flag_image(self):
        """Возврат трансформированной закрытой зафлажированной ячейки"""
        return self.__transform_cell_close_flag_image

    @property
    def transform_cell_mine_image(self):
        """Возврат трансформированной ячейки с открытой бомбой"""
        return self.__transform_cell_mine_image

    @property
    def transform_cell_open_01_image(self):
        """Возврат трансформированной открытой ячейки с цифрой 1"""
        return self.__transform_cell_open_01_image

    @property
    def transform_cell_open_02_image(self):
        """Возврат трансформированной открытой ячейки с цифрой 2"""
        return self.__transform_cell_open_02_image

    @property
    def transform_cell_open_03_image(self):
        """Возврат трансформированной открытой ячейки с цифрой 3"""
        return self.__transform_cell_open_03_image

    @property
    def transform_cell_open_04_image(self):
        """Возврат трансформированной открытой ячейки с цифрой 4"""
        return self.__transform_cell_open_04_image

    @property
    def transform_cell_open_05_image(self):
        """Возврат трансформированной открытой ячейкис цифрой 5"""
        return self.__transform_cell_open_05_image

    @property
    def transform_cell_open_06_image(self):
        """Возврат трансформированной открытой ячейки с цифрой 6"""
        return self.__transform_cell_open_06_image

    @property
    def transform_cell_open_07_image(self):
        """Возврат трансформированной открытой ячейки с цифрой 7"""
        return self.__transform_cell_open_07_image

    @property
    def transform_cell_open_08_image(self):
        """Возврат трансформированной открытой ячейки с цифрой 8"""
        return self.__transform_cell_open_08_image

    # @property
    # def transform_cell_loot_heart_image(self):
    #     """Возврат трансформированной картинки сердечка в открытой ячейке"""
    #     return self.__transform_cell_loot_heart_image

    @property
    def transform_cell_loot_heart_01_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_01_image

    @property
    def transform_cell_loot_heart_02_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_02_image

    @property
    def transform_cell_loot_heart_03_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_03_image

    @property
    def transform_cell_loot_heart_04_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_04_image

    @property
    def transform_cell_loot_heart_05_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_05_image

    @property
    def transform_cell_loot_heart_06_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_06_image

    @property
    def transform_cell_loot_heart_07_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_07_image

    @property
    def transform_cell_loot_heart_08_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_08_image

    @property
    def transform_cell_loot_heart_09_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_09_image

    @property
    def transform_cell_loot_heart_10_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_10_image

    @property
    def transform_cell_loot_heart_11_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_11_image

    @property
    def transform_cell_loot_heart_12_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_12_image

    @property
    def transform_cell_loot_heart_13_image(self):
        """Возврат трансформированной картинки сердечка в открытой ячейке"""
        return self.__transform_cell_loot_heart_13_image

    # @property
    # def transform_cell_loot_coin_0_image(self):
    #     """Возврат трансформированной картинки монеты в открытой ячейке"""
    #     return self.__transform_cell_loot_coin_0_image

    @property
    def transform_cell_loot_coin_01_image(self):
        """Возврат трансформированной картинки монеты в открытой ячейке"""
        return self.__transform_cell_loot_coin_01_image

    @property
    def transform_cell_loot_coin_02_image(self):
        """Возврат трансформированной картинки монеты в открытой ячейке"""
        return self.__transform_cell_loot_coin_02_image

    @property
    def transform_cell_loot_coin_03_image(self):
        """Возврат трансформированной картинки монеты в открытой ячейке"""
        return self.__transform_cell_loot_coin_03_image

    @property
    def transform_cell_loot_coin_04_image(self):
        """Возврат трансформированной картинки монеты в открытой ячейке"""
        return self.__transform_cell_loot_coin_04_image

    @property
    def transform_cell_loot_coin_05_image(self):
        """Возврат трансформированной картинки монеты в открытой ячейке"""
        return self.__transform_cell_loot_coin_05_image

    @property
    def transform_cell_loot_coin_06_image(self):
        """Возврат трансформированной картинки монеты в открытой ячейке"""
        return self.__transform_cell_loot_coin_06_image

    # @property
    # def transform_cell_loot_coin_7_image(self):
    #     """Возврат трансформированной картинки монеты в открытой ячейке"""
    #     return self.__transform_cell_loot_coin_7_image

    @property
    def transform_main_character_image(self):
        """Возврат трансформированной картинки персонажа"""
        return self.__transform_main_character_image

    @property
    def transform_main_character_1_image(self):
        """Возврат трансформированной картинки персонажа"""
        return self.__transform_main_character_1_image

    @property
    def transform_main_character_2_image(self):
        """Возврат трансформированной картинки персонажа"""
        return self.__transform_main_character_2_image

    @property
    def transform_main_character_3_image(self):
        """Возврат трансформированной картинки персонажа"""
        return self.__transform_main_character_3_image

    @property
    def transform_main_character_4_image(self):
        """Возврат трансформированной картинки персонажа"""
        return self.__transform_main_character_4_image

    @property
    def transform_main_character_5_image(self):
        """Возврат трансформированной картинки персонажа"""
        return self.__transform_main_character_5_image

    @property
    def transform_main_character_6_image(self):
        """Возврат трансформированной картинки персонажа"""
        return self.__transform_main_character_6_image

    @property
    def transform_explosion_00_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_00_image

    @property
    def transform_explosion_01_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_01_image

    @property
    def transform_explosion_02_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_02_image

    @property
    def transform_explosion_03_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_03_image

    @property
    def transform_explosion_04_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_04_image

    @property
    def transform_explosion_05_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_05_image

    @property
    def transform_explosion_06_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_06_image

    @property
    def transform_explosion_07_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_07_image

    @property
    def transform_explosion_08_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_08_image

    @property
    def transform_explosion_09_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_09_image

    @property
    def transform_explosion_10_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_10_image

    @property
    def transform_explosion_11_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_11_image

    @property
    def transform_explosion_12_image(self):
        """Возврат трансформированной картинки взрыва при открытии мины"""
        return self.__transform_explosion_12_image

    @property
    def transform_cell_loot_orange_image(self):
        """Возврат трансформированной картинки апельсина в открытой ячейке"""
        return self.__transform_cell_loot_orange_image

    @property
    def transform_orange_image(self):
        """Возврат трансформированной картинки апельсина"""
        return self.__transform_orange_image

    @property
    def transform_cell_loot_banana_image(self):
        """Возврат трансформированной картинки банана в открытой ячейке"""
        return self.__transform_cell_loot_banana_image

    @property
    def transform_banana_image(self):
        """Возврат трансформированной картинки банана"""
        return self.__transform_banana_image

    @property
    def transform_cell_loot_strawberry_image(self):
        """Возврат трансформированной картинки земляники в открытой ячейке"""
        return self.__transform_cell_loot_strawberry_image

    @property
    def transform_strawberry_image(self):
        """Возврат трансформированной картинки земляники"""
        return self.__transform_strawberry_image

    @property
    def transform_cell_loot_apple_image(self):
        """Возврат трансформированной картинки яблока в открытой ячейке"""
        return self.__transform_cell_loot_apple_image

    @property
    def transform_apple_image(self):
        """Возврат трансформированной картинки яблока"""
        return self.__transform_apple_image

    @property
    def transform_cell_loot_cell_opening_staff(self):
        """Возврат трансформированной картинки посоха открывающего ячейку в ячейке"""
        return self.__transform_cell_loot_cell_opening_staff

    @property
    def transform_cell_opening_staff(self):
        """Возврат трансформированной картинки посоха открывающего ячейку"""
        return self.__transform_cell_opening_staff

    @property
    def transform_cell_icon_image(self):
        """Возврат трансформированной картинки иконки ячейки"""
        return self.__transform_cell_icon_image

    @property
    def transform_restart_button_image(self):
        """Возврат трансформированной картинки кнопки перезапуска"""
        return self.__transform_restart_button_image

    # @property
    # def transform_lvl1_mini_image(self):
    #     """Возврат трансформированной картинки персонажа"""
    #     return self.__lvl1_image
