from time import time as timer


class Coin:
    """Событие, по которому монета крутится"""

    def __init__(self):
        """Инициализация"""
        self.__images = [0 for _ in range(6)]
        self.__counter = 0
        self.__index = 0
        self.__coin_speed = 4
        self.__coin_is_not_updating = True

    def update_image(self):
        """Обновление анимации. Контроллер"""
        long_update_in_sec = 0.05
        if self.__coin_is_not_updating:
            self.__coin_is_not_updating = False
            self.__update_image()
        elif timer() - self.__start_last_update_time >= long_update_in_sec:
            self.__coin_is_not_updating = True
            self.__update_image()

    def __update_image(self):
        """Обновление анимации"""
        self.__start_last_update_time = timer()
        # скорость обновления анимации монеты
        self.__counter += 1
        # переход на следующий спрайт и проверка за выход из списка изображений
        if self.__counter >= self.__coin_speed and self.__index < len(self.__images) - 1:
            self.__counter = 0
            # следующий img
            self.__index += 1
        # если анимация завершена, то экземпляр повторяется
        if self.__index >= len(self.__images) - 1:
            self.__index = 0

    @property
    def index(self):
        return self.__index
