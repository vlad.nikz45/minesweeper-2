import sys

import pygame
from mvc.model import MinesweeperModel
from packages.coin import Coin
from packages.explosion import Explosion
from packages.fonts import menu_font, average_text, small_text
from packages.heart import Heart
from packages.lvl_settings import LvlSettings, LoadLvlSettings
from packages.mixer import Mixer
from packages.musics import menu_music, level_1_music, level_2_music, level_3_music
from packages.pug import Pug
from packages.snowfall import Snowfall
from packages.transform_images import TransformImages


class Text:
    """Класс для обработки текста"""

    def __init__(self, message: str, name_font: str, size: int, color):
        """Инициализация"""
        self.__text = self.text_format(message, name_font, size, color)
        # self.text_list.append(self.__text)
        self.__message = message
        # self.exemplars.append(self)

    @staticmethod
    def text_format(message: str, name_font: str, text_size: int, text_color):
        """Переводит принятый текст в текст для Pygame"""
        pygame_font = pygame.font.Font(name_font, text_size)
        return pygame_font.render(message, False, text_color)

    @property
    def get_text(self):
        """Возвращает текст сообщения"""
        return self.__message

    @property
    def get_pygame_text(self):
        """Возвращает текст для Pygame"""
        return self.__text


class MinesweeperView:
    """Представление сапёра"""

    def __init__(self, controller):
        """Инициализация"""
        pygame.init()
        self.__screen_width = 1280
        self.__screen_height = 720
        self.__instance_class_t_i = TransformImages(self.__screen_width, self.__screen_height)
        pygame.mouse.set_cursor(pygame.cursors.diamond)
        self.__controller = controller
        self.__clock = pygame.time.Clock()
        self.__FPS = 60
        self.__min_width = 12
        self.__max_width = 120
        self.__width = self.__min_width
        self.__padding_top = self.__screen_height / 4 * 0.4
        self.__padding_left = self.__screen_width / 4 * 1
        self.__cells_collider = []
        self.__step_move = 10
        self.__was_mouse_down_pos = False
        self.__mouse_down_pos = 0
        self.__last_mouse_pos = None
        self.__is_game_continue = True
        # self.__screen = pygame.display.set_mode((self.__screen_width, self.__screen_height), pygame.FULLSCREEN)
        self.__screen = pygame.display.set_mode((self.__screen_width, self.__screen_height))
        self.__surf_down_bar_alpha = pygame.Surface((self.__screen_width, self.__screen_height * 1 / 5))
        self.__surf_down_bar_alpha.set_alpha(128)
        self.__surf_black_bar_alpha = pygame.Surface((self.__screen_width, self.__screen_height))
        self.__surf_black_bar_alpha.set_alpha(192)
        self.__surf_little_black_bar_alpha = pygame.Surface((self.__screen_width, self.__screen_height))
        self.__surf_little_black_bar_alpha.set_alpha(128)
        # self.surf_down_bar = pygame.Surface((self.__screen_width, self.__screen_height * 1 / 4))
        # self.exemplars, self.text_list = self.create_texts()
        self.__explosion_group = pygame.sprite.Group()
        self.__lvl_settings_list = [LvlSettings(*LoadLvlSettings(i + 1).parsing_lvl()) for i in range(0, 9)]
        self.__mixer = Mixer()
        self.__musics_list = [level_1_music, level_1_music, level_1_music,
                              level_2_music, level_2_music, level_2_music,
                              level_3_music, level_3_music, level_3_music]
        self.__sound_result_not_played = True
        self.__set_index_level()
        self.__level_was_not_entered = True
        self.__images_inventory_items_list = [self.__instance_class_t_i.transform_orange_image,
                                              self.__instance_class_t_i.transform_banana_image,
                                              self.__instance_class_t_i.transform_strawberry_image,
                                              self.__instance_class_t_i.transform_apple_image,
                                              self.__instance_class_t_i.transform_cell_opening_staff]
        self.__inventory_collider = []
        self.__inventory_was_not_created = True
        self.__shop_collider = []
        self.__shop_items_was_not_created = True
        self.__restart_button_collider_was_created = False
        self.__images_pug_animation_list = [self.__instance_class_t_i.transform_main_character_image,
                                            self.__instance_class_t_i.transform_main_character_image,
                                            self.__instance_class_t_i.transform_main_character_1_image,
                                            self.__instance_class_t_i.transform_main_character_1_image,
                                            self.__instance_class_t_i.transform_main_character_1_image,
                                            self.__instance_class_t_i.transform_main_character_2_image,
                                            self.__instance_class_t_i.transform_main_character_2_image,
                                            self.__instance_class_t_i.transform_main_character_2_image,
                                            self.__instance_class_t_i.transform_main_character_2_image,
                                            self.__instance_class_t_i.transform_main_character_image,
                                            self.__instance_class_t_i.transform_main_character_3_image,
                                            self.__instance_class_t_i.transform_main_character_3_image,
                                            self.__instance_class_t_i.transform_main_character_3_image,
                                            self.__instance_class_t_i.transform_main_character_3_image,
                                            self.__instance_class_t_i.transform_main_character_4_image,
                                            self.__instance_class_t_i.transform_main_character_4_image,
                                            self.__instance_class_t_i.transform_main_character_4_image,
                                            self.__instance_class_t_i.transform_main_character_4_image,
                                            self.__instance_class_t_i.transform_main_character_image,
                                            self.__instance_class_t_i.transform_main_character_image,
                                            self.__instance_class_t_i.transform_main_character_5_image,
                                            self.__instance_class_t_i.transform_main_character_6_image]
        self.__pug = Pug(len(self.__images_pug_animation_list))

    def __set_model(self, model):
        """Установка модели"""
        self.__model = model
        self.__row_number = self.__model.row_number
        self.__column_number = self.__model.column_number
        self.__mine_number = self.__model.mine_number
        self.__mine_damage = self.__model.mine_damage
        self.__hearts_drop_number = self.__model.hearts_drop_number
        self.__coins_drop_number = self.__model.coins_drop_number

        dict_width = {3: 120, 4: 120, 5: 120, 6: 110, 7: 90, 8: 80, 9: 68, 10: 60, 11: 56, 12: 52, 13: 48,
                      14: 44, 15: 40, 16: 38, 17: 36, 18: 34, 19: 32, 20: 30, 21: 28, 22: 27, 23: 26, 24: 25, 25: 25,
                      26: 25, 27: 25, 28: 25, 29: 25, 30: 25}
        if len(self.__model.cells_table) <= 30:
            self.__width = dict_width[len(self.__model.cells_table)]
        else:
            self.__width = self.__min_width

        self.__min_padding_top = -(self.__row_number * self.__width)
        self.__max_padding_top = self.__screen_height
        self.__min_padding_left = -(self.__column_number * self.__width)
        self.__max_padding_left = self.__screen_width
        self.__coins_data_list = []
        self.__hearts_data_list = []

    @property
    def hearts_data_list(self):
        """Возврат списка созданных сердец на экране"""
        return self.hearts_data_list

    @hearts_data_list.setter
    def hearts_data_list(self, value):
        """Установка значения списка созданных сердец на экране"""
        self.__hearts_data_list = value

    @property
    def coins_data_list(self):
        """Возврат списка созданных монет на экране"""
        return self.__coins_data_list

    @coins_data_list.setter
    def coins_data_list(self, value):
        """Установка значения списка созданных монет на экране"""
        self.__coins_data_list = value

    # @coins_data_list.setter
    # def coins_data_list(self, value):
    #     """Установка значения у списка созданных монет на экране"""
    #     self.coins_data_list = value

    def __set_index_level(self, index_level: int = 0):
        """Устанавливает текущий индекс уровня"""
        self.__index_level = index_level

    @property
    def index_level(self):
        """Возврат индекса нынешного уровня"""
        return self.__index_level

    @property
    def is_game_continue(self):
        """Возврат проверки на продолжение игры"""
        return self.__is_game_continue

    @is_game_continue.setter
    def is_game_continue(self, value):
        """Установка проверки на продолжение игры"""
        self.__is_game_continue = value

    @property
    def padding_left(self):
        """Отступ слева. Геттер"""
        return self.__padding_left

    @padding_left.setter
    def padding_left(self, value):
        """Отступ слева. Сеттер"""
        if self.__min_padding_left <= self.__padding_left <= self.__max_padding_left:
            self.__padding_left = value
        if self.__min_padding_left >= self.__padding_left:
            self.__padding_left = self.__min_padding_left
        elif self.__max_padding_left <= self.__padding_left:
            self.__padding_left = self.__max_padding_left

    @property
    def padding_top(self):
        """Отступ сверху. Геттер"""
        return self.__padding_top

    @padding_top.setter
    def padding_top(self, value):
        """Отступ сверху. Сеттер"""
        if self.__min_padding_top <= self.__padding_top <= self.__max_padding_top:
            self.__padding_top = value
        if self.__min_padding_top >= self.__padding_top:
            self.__padding_top = self.__min_padding_top
        elif self.__max_padding_top <= self.__padding_top:
            self.__padding_top = self.__max_padding_top

    @property
    def level_was_not_entered(self):
        """Возврат атрибута что в уровень не входили"""
        return self.__level_was_not_entered

    @level_was_not_entered.setter
    def level_was_not_entered(self, value):
        """Установка атрибута что в уровень не входили"""
        self.__level_was_not_entered = value

    @property
    def sound_result_not_played(self):
        """Возврат атрибута что звук результата игры не проигрывался"""
        return self.__sound_result_not_played

    @sound_result_not_played.setter
    def sound_result_not_played(self, value):
        """Установка атрибута что звук результата игры не проигрывался"""
        self.__sound_result_not_played = value

    def get_screen_width_and_screen_height(self):
        """Получение целевого разрещения"""
        return self.__screen_width, self.__screen_height

    @staticmethod
    def create_texts():
        """Создание текстов"""
        start_text = Text("Начать игру", menu_font, 75, 'white')
        shop_text = Text("Магазин", menu_font, 75, 'white')
        quit_text = Text("Выйти", menu_font, 75, 'white')
        return [start_text, shop_text, quit_text], [start_text.get_pygame_text,
                                                    shop_text.get_pygame_text,
                                                    quit_text.get_pygame_text]

    def create_texts_choose_lvl(self):
        """Создание текста для меню выбора уровня
         (возвращает текст, количество бомб, урон, количество клеток на уровнях)"""
        headlines = []
        count_bombs = []
        lvl_damage = []
        cells_count = []
        for i in range(len(self.__lvl_settings_list)):
            headlines.append(Text(self.__lvl_settings_list[i].get_headline, average_text, 70, 'white').get_pygame_text)
            count_bombs.append(Text(self.__lvl_settings_list[i].get_bomb_num, average_text, 35, 'white'))
            lvl_damage.append(Text(self.__lvl_settings_list[i].get_bomb_damage_for_text, average_text, 35, 'white'))
            cells_count.append(
                Text(
                    str(int(self.__lvl_settings_list[i].get_row_num) * int(self.__lvl_settings_list[i].get_column_num)),
                    average_text, 35, 'white'))

        # return [lvl1_headline.get_pygame_text], [lvl1_count_bombs], [lvl1_damage], [lvl1_cell_count]
        return headlines, count_bombs, lvl_damage, cells_count

    # def create_gui(self):
    #     """Создание графического интерфейса"""
    #     rectangle_down_bar = pygame.rect.Rect(0, 0, self.__screen_width, self.__screen_height * 1 / 4)
    #     pygame.draw.rect(self.__surf_down_bar_alpha, BLACK, rectangle_down_bar)

    def draw_gui(self):
        """Отрисовка созданного графического интерфейса снизу"""

        self.__screen.blit(self.__surf_down_bar_alpha, (0, self.__screen_height * 6 / 7))
        # self.__screen.blit(self.__instance_class_t_i.down_panel_image, (0, self.__screen_height / 1.2))
        self.__pug.update_image()
        self.__screen.blit(self.__images_pug_animation_list[self.__pug.index],
                           (self.__screen_height / 20, self.__screen_height * 6 / 7))
        # Здоровье
        self.__screen.blit(self.__instance_class_t_i.hp_icon_image,
                           (self.__screen_width / 7, self.__screen_height * 9 / 10))
        self.__screen.blit(
            Text(str(self.__controller.current_hp_player()) + f' / {self.__controller.max_hp_player()}', small_text, 50,
                 'white').get_pygame_text,
            (self.__screen_width / 5.5, self.__screen_height * 8.95 / 10))

        # Монеты
        self.__screen.blit(self.__instance_class_t_i.coin_icon_image,
                           (self.__screen_width / 3.5, self.__screen_height * 9 / 10))

        self.__screen.blit(Text(str(self.__controller.current_coins_player()), small_text, 50, 'white').get_pygame_text,
                           (self.__screen_width / 2.95, self.__screen_height * 8.95 / 10))

        # Инвентарь
        inventory = self.__controller.current_inventory_player()

        for data in range(4):
            self.__screen.blit(self.__instance_class_t_i.transform_cell_icon_image,
                               (self.__screen_width * 3 / 4 + data * 80,
                                self.__screen_height * 8.85 / 10))
            if inventory[data] != 0:
                self.__screen.blit(self.__images_inventory_items_list[inventory[data] - 1],
                                   (self.__screen_width * 3.04 / 4 + data * 80,
                                    self.__screen_height * 9 / 10))

            if self.__inventory_was_not_created:
                rectangle = pygame.rect.Rect(self.__screen_width * 3 / 4 + data * 80,
                                             self.__screen_height * 8.85 / 10, self.__screen_width / 20,
                                             self.__screen_width / 20)
                self.__inventory_collider.append(rectangle)
        self.__inventory_was_not_created = False
        self.__screen.blit(self.__instance_class_t_i.go_to_menu_image,
                           (self.__screen_width / 1.09, self.__screen_height / 20))
        self.__go_to_main_menu_rect = pygame.rect.Rect(self.__screen_width / 1.09, self.__screen_height / 20,
                                                       self.__screen_width / 18,
                                                       self.__screen_width / 20)

    def update_image_in_cell(self, row: int, column: int, transform_image_cell, do_pygame_update: bool = False,
                             more_padding: float = 0):
        """Обновить картинку в ячейке"""
        draw_cell = self.__screen.blit(transform_image_cell,
                                       (column * self.__width + self.padding_left + more_padding * self.__width,
                                        row * self.__width + self.padding_top + more_padding * self.__width))
        if do_pygame_update:
            pygame.display.update(draw_cell)

    def __update_board(self):
        """Отрисовка доски каждым кадром"""

        # Индексы для работы со списком coins_data_list
        coin_index = 0
        row_index = 1
        column_index = 2

        # Индекс для работы со списком __hearts_data_list
        heart_index = 0

        self.update_collider()

        self.__min_padding_left = -(self.__column_number * self.__width)
        self.__min_padding_top = -(self.__row_number * self.__width)
        self.__instance_class_t_i.calculate_transformation_cells(self.__width, self.__index_level)
        self.coins_transform_list = [self.__instance_class_t_i.transform_cell_loot_coin_01_image,
                                     self.__instance_class_t_i.transform_cell_loot_coin_02_image,
                                     self.__instance_class_t_i.transform_cell_loot_coin_03_image,
                                     self.__instance_class_t_i.transform_cell_loot_coin_04_image,
                                     self.__instance_class_t_i.transform_cell_loot_coin_05_image,
                                     self.__instance_class_t_i.transform_cell_loot_coin_06_image]
        self.boom_transform_list = [self.__instance_class_t_i.transform_explosion_00_image,
                                    self.__instance_class_t_i.transform_explosion_01_image,
                                    self.__instance_class_t_i.transform_explosion_02_image,
                                    self.__instance_class_t_i.transform_explosion_03_image,
                                    self.__instance_class_t_i.transform_explosion_04_image,
                                    self.__instance_class_t_i.transform_explosion_05_image,
                                    self.__instance_class_t_i.transform_explosion_06_image,
                                    self.__instance_class_t_i.transform_explosion_07_image,
                                    self.__instance_class_t_i.transform_explosion_08_image,
                                    self.__instance_class_t_i.transform_explosion_09_image,
                                    self.__instance_class_t_i.transform_explosion_10_image,
                                    self.__instance_class_t_i.transform_explosion_11_image,
                                    self.__instance_class_t_i.transform_explosion_12_image]
        self.hearts_transform_list = [self.__instance_class_t_i.transform_cell_loot_heart_01_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_02_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_03_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_04_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_05_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_06_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_07_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_08_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_09_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_10_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_11_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_12_image,
                                      self.__instance_class_t_i.transform_cell_loot_heart_13_image]
        for coin in self.__coins_data_list:
            coin[coin_index].update_image()

        for heart in self.__hearts_data_list:
            heart[heart_index].update_image()

        for row in range(len(self.__model.cells_table)):
            for column in range(len(self.__model.cells_table[row])):
                cell = self.__model.get_cell(row, column)
                if cell:
                    if cell.state == 'closed':
                        if not cell.was_not_flagged:
                            cell.was_not_flagged = True
                        self.update_image_in_cell(row, column,
                                                  self.__instance_class_t_i.transform_cell_close_image)
                    elif cell.state == 'flagged':
                        if cell.was_not_flagged:
                            cell.was_not_flagged = False
                            self.__mixer.play_put_flag_sound(0.1)
                        self.update_image_in_cell(row, column,
                                                  self.__instance_class_t_i.transform_cell_close_flag_image)
                    elif cell.state == 'opened':
                        if cell.mined:
                            if cell.boom_not_created:
                                explosion = Explosion(self.boom_transform_list,
                                                      column * self.__width + self.padding_left + self.__width / 2,
                                                      row * self.__width + self.padding_top + self.__width / 2)
                                self.__explosion_group.add(explosion)
                                self.__mixer.play_boom_sound(0.03)
                                cell.boom_not_created = False
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_mine_image)
                        elif cell.counter == 1:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_01_image)
                        elif cell.counter == 2:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_02_image)
                        elif cell.counter == 3:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_03_image)
                        elif cell.counter == 4:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_04_image)
                        elif cell.counter == 5:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_05_image)
                        elif cell.counter == 6:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_06_image)
                        elif cell.counter == 7:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_07_image)
                        elif cell.counter == 8:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_08_image)
                        else:
                            self.update_image_in_cell(row, column,
                                                      self.__instance_class_t_i.transform_cell_open_image)
                        if cell.have_heart:
                            if cell.heart_not_created:
                                heart = Heart()
                                self.__hearts_data_list.append([heart, row, column])
                                cell.heart_not_created = False
                            for index in range(len(self.__hearts_data_list)):
                                if self.__hearts_data_list[index][row_index] == row \
                                        and self.__hearts_data_list[index][column_index] == column:
                                    index_image = self.__hearts_data_list[index][heart_index].index
                                    self.update_image_in_cell(row, column, self.hearts_transform_list[index_image],
                                                              False, 0.22)
                            # self.update_image_in_cell(row, column,
                            #                           self.__instance_class_t_i.transform_cell_loot_heart_image)
                        elif cell.have_coin:
                            if cell.coin_not_created:
                                coin = Coin()
                                self.__coins_data_list.append([coin, row, column])
                                cell.coin_not_created = False
                            for index in range(len(self.__coins_data_list)):
                                if self.__coins_data_list[index][row_index] == row \
                                        and self.__coins_data_list[index][column_index] == column:
                                    index_image = self.__coins_data_list[index][coin_index].index
                                    self.update_image_in_cell(row, column, self.coins_transform_list[index_image],
                                                              False, 0.22)
                        elif cell.have_inventory_item:
                            if cell.id_inventory_item == 1:
                                self.update_image_in_cell(row, column,
                                                          self.__instance_class_t_i.transform_cell_loot_orange_image,
                                                          False, 0.22)
                            elif cell.id_inventory_item == 2:
                                self.update_image_in_cell(row, column,
                                                          self.__instance_class_t_i.transform_cell_loot_banana_image,
                                                          False, 0.22)
                            elif cell.id_inventory_item == 3:
                                self.update_image_in_cell(row, column,
                                                          self.__instance_class_t_i.transform_cell_loot_strawberry_image,
                                                          False, 0.22)
                            elif cell.id_inventory_item == 4:
                                self.update_image_in_cell(row, column,
                                                          self.__instance_class_t_i.transform_cell_loot_apple_image,
                                                          False, 0.22)
                            elif cell.id_inventory_item == 5:
                                self.update_image_in_cell(row, column,
                                                          self.__instance_class_t_i.transform_cell_loot_cell_opening_staff,
                                                          False, 0.22)
                        if cell.was_not_opened:
                            cell.was_not_opened = False
                            self.__mixer.play_open_cell_sound(0.2)
        self.__explosion_group.draw(self.__screen)
        self.__explosion_group.update()
        self.__snow_fall.start_snowfall()
        self.draw_gui()
        self.draw_gui_states_level()

        if not self.__is_game_continue:
            self.draw_lose_menu()
        if self.__model.is_win() and True not in self.__controller.current_lvl_is_not_complete_list():
            self.draw_absolute_win()
        elif self.__model.is_win():
            self.draw_win_menu()

        pygame.display.update()

    def draw_gui_states_level(self):
        """Отрисовка статистик уровня на уровне"""
        self.__screen.blit(Text("Бомб осталось: " + str(self.__model.how_much_mines_left()),
                                small_text, int(self.__screen_width / 25), 'white').get_pygame_text,
                           (self.__screen_width / 150, self.__screen_height * 1 / 30))
        self.__screen.blit(Text("Урон бомб: " + str(self.__model.mine_damage),
                                small_text, int(self.__screen_width / 25), 'white').get_pygame_text,
                           (self.__screen_width / 150, self.__screen_height * 4 / 30))

    def draw_lose_menu(self):
        """Отрисовка меню поражения"""
        self.__screen.blit(self.__surf_black_bar_alpha, (0, 0))
        self.__screen.blit(self.__instance_class_t_i.lose_image,
                           (self.__screen_width / 4, self.__screen_height / 10))
        self.__screen.blit(self.__instance_class_t_i.transform_restart_button_image,
                           (self.__screen_width * 4 / 9, self.__screen_height / 2))
        self.__restart_button_collider = pygame.rect.Rect(self.__screen_width * 4 / 9, self.__screen_height / 2,
                                                          self.__screen_width / 7, self.__screen_width / 7)
        self.__restart_button_collider_was_created = True

    def draw_win_menu(self):
        """Отрисовка меню победы уровня"""
        self.__screen.blit(self.__surf_little_black_bar_alpha, (0, 0))
        self.__screen.blit(Text("Вы прошли уровень! Переходите на следующий",
                                small_text, int(self.__screen_width / 25), 'white').get_pygame_text,
                           (self.__screen_width * 1 / 4, self.__screen_height * 1 / 8))
        self.__screen.blit(self.__instance_class_t_i.go_to_menu_image,
                           (self.__screen_width / 1.09, self.__screen_height / 20))

    def draw_absolute_win(self):
        """Отрисовка абсолютной победы"""
        self.__screen.blit(self.__surf_little_black_bar_alpha, (0, 0))
        self.__screen.blit(Text("Вы прошли игру! Поздравляем! Спасибо за прохождение!",
                                small_text, int(self.__screen_width / 25), 'white').get_pygame_text,
                           (self.__screen_width * 1 / 6, self.__screen_height * 5.5 / 8))
        self.__screen.blit(Text("Авторы: Владислав Никитин, Сергей Вдовин, Александр Домненко, Павел Орлов,"
                                " Даниил Ботвенко, Сергей Сусликов",
                                small_text, int(self.__screen_width / 50), 'white').get_pygame_text,
                           (self.__screen_width * 1.5 / 10, self.__screen_height * 41 / 43))
        self.__screen.blit(self.__instance_class_t_i.go_to_menu_image,
                           (self.__screen_width / 1.09, self.__screen_height / 20))
        self.__screen.blit(self.__instance_class_t_i.win_image,
                           (self.__screen_width / 4, self.__screen_height / 10))

    def create_collider_rectangles_cells(self):
        """Создание коллайдеров для ячеек"""
        self.__cells_collider = []
        for row in range(len(self.__model.cells_table)):
            for column in range(len(self.__model.cells_table[row])):
                rectangle = pygame.rect.Rect(column * self.__width + self.padding_left,
                                             row * self.__width + self.padding_top, self.__width, self.__width)
                self.__cells_collider.append(rectangle)

    def update_collider(self):
        """Перерасчёт коллайдеров с учётом расположения ячейки"""
        # transform_image_cell = pygame.transform.scale(bg_lvl, (self.__screen_width, self.__screen_height))
        self.__screen.blit(self.__instance_class_t_i.transform_bg_lvl_image, (0, 0))
        self.create_collider_rectangles_cells()

    def focus_block(self, row: int, column: int):
        """Наведение на блок ячейки"""
        self.__instance_class_t_i.calculate_focus_close_cell(self.__width, self.__index_level)
        cell = self.__model.get_cell(row, column)
        if cell:
            if cell.state == 'closed':
                self.update_image_in_cell(row, column, self.__instance_class_t_i.transform_cell_close_image,
                                          True)

    # def show_level_game_over_menu(self):
    #     """Показ меню окончания уровня"""
    #     self.__screen.blit(self.__instance_class_t_i.transform_bg_lvl_image, (0, 0))
    #     print('КОНЕЦ')

    def show_level(self):
        """Показ уровня"""
        # self.create_gui()
        self.__update_board()
        show_level_is_running = True

        left_mouse_button = 1
        central_mouse_button = 2
        right_mouse_button = 3

        while show_level_is_running:

            keys = pygame.key.get_pressed()
            if keys[pygame.K_LEFT]:
                self.padding_left -= self.__step_move
            if keys[pygame.K_RIGHT]:
                self.padding_left += self.__step_move
            if keys[pygame.K_UP]:
                self.padding_top -= self.__step_move
            if keys[pygame.K_DOWN]:
                self.padding_top += self.__step_move

            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONUP and event.button == left_mouse_button:
                    if self.__go_to_main_menu_rect.collidepoint(event.pos):
                        self.__mixer.play_menu_select_sound(0.3)
                        self.show_menu_lvl()
                if event.type == pygame.QUIT:
                    self.show_menu_lvl()

                if event.type == pygame.MOUSEMOTION:
                    self.__last_mouse_pos = event.pos
                    for i in range(len(self.__cells_collider)):
                        if self.__cells_collider[i].collidepoint(event.pos) and self.__is_game_continue:
                            row = i // self.__column_number
                            column = i % self.__column_number
                            self.focus_block(row, column)

                    do_change = False
                    if self.__was_mouse_down_pos:
                        if abs(event.pos[0] - self.__mouse_down_pos[0]) > 3:
                            self.padding_left += (int((event.pos[0] - self.__mouse_down_pos[0]) >= 0) - 0.5) * 2 \
                                                 * self.__step_move
                            do_change = True
                        if abs(event.pos[1] - self.__mouse_down_pos[1]) > 3:
                            self.padding_top += (int((event.pos[1] - self.__mouse_down_pos[1]) >= 0) - 0.5) * 2 \
                                                * self.__step_move
                            do_change = True
                        if do_change:
                            self.__mouse_down_pos = event.pos
                            self.__was_mouse_down_pos = True

                if event.type == pygame.MOUSEBUTTONDOWN and event.button == central_mouse_button:
                    self.__mouse_down_pos = event.pos
                    self.__was_mouse_down_pos = True

                if event.type == pygame.MOUSEWHEEL:
                    self.__width += event.y * 2
                    if self.__width < self.__min_width:
                        self.__width = self.__min_width
                    if self.__width > self.__max_width:
                        self.__width = self.__max_width
                    if self.__width != self.__min_width and self.__width != self.__max_width:
                        multiply_number_padding_top = 0.2
                        if self.__last_mouse_pos[0] >= self.__screen_width / 2 > 0:
                            multiply_number_padding_left = 0.8
                        else:
                            multiply_number_padding_left = 0.4
                        self.padding_top -= ((int(self.__last_mouse_pos[
                                                      1] - self.__screen_height / 2 > 0) - 0.5) * self.__width
                                             * multiply_number_padding_top)
                        self.padding_left -= ((int(self.__last_mouse_pos[
                                                       0] - self.__screen_width / 2 > 0) - 0.5) * self.__width
                                              * multiply_number_padding_left)

                        if event.y != abs(event.y) and self.__last_mouse_pos[0] > self.__screen_width / 2:
                            # self.padding_left += self.__width * multiply_number_padding_left * 3
                            self.__last_mouse_pos = (
                                self.__last_mouse_pos[0] + self.__width * multiply_number_padding_left,
                                self.__last_mouse_pos[1])
                        elif event.y != abs(event.y) and self.__last_mouse_pos[0] <= self.__screen_width / 2:
                            # self.padding_left -= self.__width * multiply_number_padding_left * 3
                            self.__last_mouse_pos = (
                                self.__last_mouse_pos[0] - self.__width * multiply_number_padding_left,
                                self.__last_mouse_pos[1])
                        if event.y != abs(event.y) and self.__last_mouse_pos[1] > self.__screen_height / 2:
                            # self.padding_top += self.__width * multiply_number_padding_top * 12
                            self.__last_mouse_pos = (
                                self.__last_mouse_pos[0],
                                self.__last_mouse_pos[1] + self.__width * multiply_number_padding_top)
                        elif event.y != abs(event.y) and self.__last_mouse_pos[1] <= self.__screen_height / 2:
                            # self.padding_top -= self.__width * multiply_number_padding_top * 12
                            self.__last_mouse_pos = (
                                self.__last_mouse_pos[0],
                                self.__last_mouse_pos[1] - self.__width * multiply_number_padding_top)

                if event.type == pygame.MOUSEBUTTONUP and event.button == central_mouse_button:
                    self.__was_mouse_down_pos = False

                if event.type == pygame.MOUSEBUTTONUP and event.button == left_mouse_button \
                        and self.__is_game_continue is not True:
                    if self.__restart_button_collider_was_created:
                        self.click_on_restart_button(event)

                if event.type == pygame.MOUSEBUTTONUP and event.button == left_mouse_button and self.__is_game_continue:
                    self.click_on_inventory(event, 'lvl')
                    for i in range(len(self.__cells_collider)):
                        if self.__cells_collider[i].collidepoint(event.pos):
                            row = i // self.__column_number
                            column = i % self.__column_number
                            cell = self.__model.get_cell(row, column)
                            if cell:
                                if cell.state == 'opened' and not cell.mined:
                                    self.__controller.on_left_click_opened(cell.counter, row, column, cell.have_heart,
                                                                           cell.have_coin, self.__mixer,
                                                                           cell.have_inventory_item,
                                                                           cell.id_inventory_item)
                                elif cell.state != 'flagged':
                                    self.__controller.on_left_click(row, column)

                if event.type == pygame.MOUSEBUTTONUP and event.button == right_mouse_button \
                        and self.__is_game_continue:
                    for i in range(len(self.__cells_collider)):
                        if self.__cells_collider[i].collidepoint(event.pos):
                            row = i // self.__column_number
                            column = i % self.__column_number
                            cell = self.__model.get_cell(row, column)
                            if cell:
                                if cell.state == 'opened':
                                    self.__controller.on_right_click_opened(cell.counter, row, column)
                                else:
                                    self.__controller.on_right_click(row, column)
                    self.show_how_much_mines_left()

            self.__update_board()

    def click_on_restart_button(self, event):
        """Нажатие на кнопку перезапуска"""
        if self.__restart_button_collider.collidepoint(event.pos):
            self.__restart_button_collider_was_created = False
            self.__mixer.play_menu_select_sound(0.3)
            self.__controller.on_left_click_on_restart()

    def get_game_settings(self):
        """Получение игровых настроек"""
        return self.__row_number, self.__column_number, self.__mine_number, self.__mine_damage, \
               self.__hearts_drop_number, self.__coins_drop_number

    def create_rects_for_mini_lvl_images(self):
        """Создание коллайдеров картинок миниатюр уровня"""
        lvl1_rect = pygame.rect.Rect(self.__screen_width / 30, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl2_rect = pygame.rect.Rect(self.__screen_width / 10, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl3_rect = pygame.rect.Rect(self.__screen_width / 6, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl4_rect = pygame.rect.Rect(self.__screen_width / 4.3, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl5_rect = pygame.rect.Rect(self.__screen_width / 3.35, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl6_rect = pygame.rect.Rect(self.__screen_width / 2.72, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl7_rect = pygame.rect.Rect(self.__screen_width / 2.3, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl8_rect = pygame.rect.Rect(self.__screen_width / 2, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)
        lvl9_rect = pygame.rect.Rect(self.__screen_width / 1.76, self.__screen_height / 6, self.__screen_width / 12,
                                     self.__screen_width / 12)

        return [lvl1_rect, lvl2_rect, lvl3_rect, lvl4_rect, lvl5_rect, lvl6_rect, lvl7_rect, lvl8_rect, lvl9_rect]

    def create_rects_for_big_lvl_images(self):
        """Создание коллайдеров картинок максиатюр уровня"""
        lvl_big_rect = pygame.rect.Rect(self.__screen_width / 30, self.__screen_height / 3, self.__screen_width / 4,
                                        self.__screen_width / 4)
        return [lvl_big_rect]

    def show_menu_lvl(self, lvl_num=0):
        """Отрисовка меню уровня"""
        left_mouse_button = 1
        lvl_titles, lvl_bomb_count, lvl_damage, lvl_cell_count = self.create_texts_choose_lvl()
        self.__screen.blit(self.__instance_class_t_i.lvl_menu_bg, (0, 0))
        title = Text("Выбор уровня", menu_font, 60, 'white')
        self.main_menu_text_draw([title.get_pygame_text], self.main_menu_text([title.get_pygame_text]), 0)
        # Какой уровень выбран
        if lvl_num == 0:
            self.__screen.blit(self.__instance_class_t_i.lvl1_image_enabled,
                               (self.__screen_width / 30, self.__screen_height / 6))
            print('Первый лвл')
            self.__screen.blit(self.__instance_class_t_i.lvl1_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl1_image,
                               (self.__screen_width / 30, self.__screen_height / 6))

        if lvl_num == 1:
            print('2 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl1_image_enabled,
                               (self.__screen_width / 10, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl1_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl1_image,
                               (self.__screen_width / 10, self.__screen_height / 6))
        if lvl_num == 2:
            print('3 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl1_image_enabled,
                               (self.__screen_width / 6, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl1_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl1_image,
                               (self.__screen_width / 6, self.__screen_height / 6))
        if lvl_num == 3:
            print('4 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl2_image_enabled,
                               (self.__screen_width / 4.3, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl2_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl2_image,
                               (self.__screen_width / 4.3, self.__screen_height / 6))
        if lvl_num == 4:
            print('5 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl2_image_enabled,
                               (self.__screen_width / 3.35, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl2_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl2_image,
                               (self.__screen_width / 3.35, self.__screen_height / 6))
        if lvl_num == 5:
            print('6 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl2_image_enabled,
                               (self.__screen_width / 2.72, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl2_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl2_image,
                               (self.__screen_width / 2.72, self.__screen_height / 6))
        if lvl_num == 6:
            print('7 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl3_image_enabled,
                               (self.__screen_width / 2.3, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl3_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl3_image,
                               (self.__screen_width / 2.3, self.__screen_height / 6))
        if lvl_num == 7:
            print('8 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl3_image_enabled,
                               (self.__screen_width / 2, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl3_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl3_image,
                               (self.__screen_width / 2, self.__screen_height / 6))
        if lvl_num == 8:
            print('9 lvl')
            self.__screen.blit(self.__instance_class_t_i.lvl3_image_enabled,
                               (self.__screen_width / 1.76, self.__screen_height / 6))
            self.__screen.blit(self.__instance_class_t_i.lvl3_image_big,
                               (self.__screen_width / 30, self.__screen_height / 3))
        else:
            self.__screen.blit(self.__instance_class_t_i.lvl3_image,
                               (self.__screen_width / 1.76, self.__screen_height / 6))

        # Отрисовка интерфейса снизу
        self.draw_gui()
        menu_lvl_is_running = True

        self.__screen.blit(self.__instance_class_t_i.lvl_ball_image,
                           (self.__screen_width / 3.4, self.__screen_height / 2.8))
        pygame.display.update()
        image_rects_mini = self.create_rects_for_mini_lvl_images()
        image_rects_big = self.create_rects_for_big_lvl_images()
        # Заголовок уровня
        self.__screen.blit(lvl_titles[lvl_num],
                           (self.__screen_width / 3, self.__screen_height / 3))
        # Количество бомб
        self.__screen.blit(self.__instance_class_t_i.bomb_counter,
                           (self.__screen_width / 3, self.__screen_height / 2.2))
        self.__screen.blit(lvl_bomb_count[lvl_num].get_pygame_text,
                           (self.__screen_width / 2.55, self.__screen_height / 2.12))
        # Иконка урона
        self.__screen.blit(self.__instance_class_t_i.bomb_damage,
                           (self.__screen_width / 3, self.__screen_height / 1.8))
        self.__screen.blit(lvl_damage[lvl_num].get_pygame_text,
                           (self.__screen_width / 2.63, self.__screen_height / 1.74))
        # сколько клеток
        self.__screen.blit(self.__instance_class_t_i.cells_count,
                           (self.__screen_width / 3, self.__screen_height / 1.52))
        self.__screen.blit(lvl_cell_count[lvl_num].get_pygame_text,
                           (self.__screen_width / 2.6, self.__screen_height / 1.48))
        # Кнопка вернуться назад в меню
        print(self.__instance_class_t_i.go_to_menu_image)
        # self.__screen.blit(self.__instance_class_t_i.go_to_menu_image,
        #                    (self.__screen_width / 1.05, self.__screen_height / 20))
        # go_to_main_menu_rect = pygame.rect.Rect(self.__screen_width / 1.05, self.__screen_height / 20, self.__screen_width / 30,
        #                              self.__screen_width / 30)
        pygame.display.update()
        while menu_lvl_is_running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.show_main_menu()
                if event.type == pygame.MOUSEBUTTONUP and event.button == left_mouse_button:
                    if self.__go_to_main_menu_rect.collidepoint(event.pos):
                        self.__mixer.play_menu_select_sound(0.3)
                        self.show_main_menu()
                    for j in image_rects_big:
                        if j.collidepoint(event.pos):
                            if self.__controller.current_lvl_is_not_complete_list()[lvl_num]:
                                self.__mixer.play_menu_select_sound(0.3)

                                if self.__level_was_not_entered:
                                    self.create_lvl(lvl_num)
                                    self.__level_was_not_entered = False
                                if lvl_num == self.__index_level:
                                    self.__mixer.set_music(self.__musics_list[lvl_num], 0.05)
                                    print("Вхожу в уровень")
                                    self.show_level()
                                else:
                                    print("Я прохожу другой уровень")
                            else:
                                self.__mixer.play_menu_select_sound(0.3)
                                print("Я уже прошёл этот уровень")
                        # тут надо будет запустить нужный уровень
                    for i in range(len(image_rects_mini)):
                        if image_rects_mini[i].collidepoint(event.pos):
                            self.__mixer.play_menu_select_sound(0.3)
                            self.show_menu_lvl(i)

                    self.click_on_inventory(event, 'menu')

    def click_on_inventory(self, event, where):
        """Нажатие на инвентарь"""
        if self.__is_game_continue:
            for i in range(len(self.__inventory_collider)):
                if self.__inventory_collider[i].collidepoint(event.pos):
                    self.__controller.on_left_click_on_cell_inventory(i, self.__mixer)
                    if where == 'menu':
                        self.show_menu_lvl()

    def create_lvl(self, index_lvl):
        """Создание уровня"""
        # print(self.__lvl_settings_list[index_lvl].get_bomb_damage)
        self.__set_index_level(index_lvl)
        if self.__index_level == 0 or self.__index_level == 1 or self.__index_level == 2:
            self.__instance_class_t_i.randomize_images("snowflakes/snowflake", 4)
            self.__snow_fall = Snowfall(self.__screen, self.__screen_width, self.__screen_height,
                                        self.__instance_class_t_i.snowflakes_images_list, 150)
        if self.__index_level == 3 or self.__index_level == 4 or self.__index_level == 5:
            self.__instance_class_t_i.randomize_images("dust/level_02", 2)
            self.__snow_fall = Snowfall(self.__screen, self.__screen_width, self.__screen_height,
                                        self.__instance_class_t_i.snowflakes_images_list, 50, 0.1, 7)
        if self.__index_level == 6 or self.__index_level == 7 or self.__index_level == 8:
            self.__instance_class_t_i.randomize_images("snowflakes/snowflake", 4)
            self.__snow_fall = Snowfall(self.__screen, self.__screen_width, self.__screen_height,
                                        self.__instance_class_t_i.snowflakes_images_list, 100, 1)
        self.__instance_class_t_i.calc_transformation_background_level(index_lvl)
        model = MinesweeperModel(int(self.__lvl_settings_list[index_lvl].get_row_num),
                                 int(self.__lvl_settings_list[index_lvl].get_column_num),
                                 int(self.__lvl_settings_list[index_lvl].get_bomb_num),
                                 eval(self.__lvl_settings_list[index_lvl].get_bomb_damage),
                                 int(self.__lvl_settings_list[index_lvl].get_hearts_drop_num),
                                 int(self.__lvl_settings_list[index_lvl].get_coins_drop_num))
        # model = MinesweeperModel(row_number, column_number, mine_number, mine_damage, hearts_drop_number,
        #                          coins_drop_number)
        self.__controller.set_model(model)
        self.__set_model(model)
        self.__sound_result_not_played = True

    def show_main_menu(self):
        """Запуск меню"""
        self.__mixer.set_music(menu_music, 0.15)
        exemplars, text_list = self.create_texts()
        main_menu_is_running = True
        self.__screen.blit(self.__instance_class_t_i.transform_bg_start_screen_image, (0, 0))
        pygame.display.update()
        text_rectangles = self.main_menu_text(text_list)
        self.main_menu_text_draw(text_list, text_rectangles, 100)
        self.__screen.blit(self.__instance_class_t_i.game_article,
                           (self.__screen_width / 30, self.__screen_height / 20))
        left_mouse_button = 1

        while main_menu_is_running:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()
                if event.type == pygame.MOUSEBUTTONUP and event.button == left_mouse_button:
                    for i in range(len(text_rectangles)):
                        if text_rectangles[i].collidepoint(event.pos):
                            print(text_list)
                            if exemplars[i].get_text == 'Выйти':
                                print('Выход')
                                self.__mixer.play_menu_select_sound(0.3)
                                sys.exit(pygame.quit())
                            if exemplars[i].get_text == 'Начать игру':
                                print(text_list[i])
                                # self.show_level()
                                self.__mixer.play_menu_select_sound(0.3)
                                self.show_menu_lvl()
                            if exemplars[i].get_text == 'Магазин':
                                self.__mixer.play_menu_select_sound(0.3)
                                self.show_shop()

            pygame.display.update()
            self.__clock.tick(self.__FPS)
            pygame.display.set_caption("Pug Traveller")

        # pygame.quit()
        # quit()

    def click_on_items_in_shop(self, event):
        """Нажатие на предметы в магазине"""
        if self.__is_game_continue:
            for i in range(len(self.__shop_collider)):
                if self.__shop_collider[i].collidepoint(event.pos):
                    self.__controller.on_left_click_on_cell_shop(i, self.__mixer)

    def show_shop(self):
        """Магазин в главном меню"""
        # self.__images_inventory_items_list = [self.__instance_class_t_i.transform_orange_image,
        #                               self.__instance_class_t_i.transform_banana_image,
        #                               self.__instance_class_t_i.transform_strawberry_image,
        #                               self.__instance_class_t_i.transform_apple_image]
        shop_menu_running = True
        left_mouse_button = 1
        pygame.display.update()
        while shop_menu_running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.show_main_menu()
                if event.type == pygame.MOUSEBUTTONUP and event.button == left_mouse_button:
                    if self.__go_to_main_menu_rect.collidepoint(event.pos):
                        self.__mixer.play_menu_select_sound(0.3)
                        self.show_main_menu()
                    self.click_on_items_in_shop(event)
                    self.click_on_inventory(event, 'lvl')

            self.__screen.blit(self.__instance_class_t_i.shop_bg, (0, 0))
            self.__screen.blit(Text("Доступные предметы", menu_font, 70, 'white').get_pygame_text,
                               (self.__screen_width / 4, self.__screen_height / 8))
            self.draw_gui()
            self.draw_shop_items()

    def draw_shop_items(self):
        """Отрисовка продаваемых предметов в магазине"""
        shop_items = self.__controller.current_shop_items()
        cost_items_shop = self.__controller.current_cost_items_shop()

        for data in range(len(shop_items)):
            self.__screen.blit(self.__instance_class_t_i.transform_cell_icon_image,
                               (self.__screen_width * 3.5 / 10 + data * 80,
                                self.__screen_height * 3 / 10))
            if shop_items[data] != 0:
                self.__screen.blit(self.__images_inventory_items_list[shop_items[data] - 1],
                                   (self.__screen_width * 3.6 / 10 + data * 80,
                                    self.__screen_height * 3.16 / 10))

                self.__screen.blit(Text(str(cost_items_shop[data]), menu_font, 35, 'white').get_pygame_text,
                                   (self.__screen_width * 3.7 / 10 + data * 80,
                                    self.__screen_height * 3.86 / 10))

            if self.__shop_items_was_not_created:
                rectangle = pygame.rect.Rect(self.__screen_width * 3.5 / 10 + data * 80,
                                             self.__screen_height * 3 / 10, self.__screen_width / 20,
                                             self.__screen_width / 20)
                self.__shop_collider.append(rectangle)
        self.__shop_items_was_not_created = False
        pygame.display.update()

    def main_menu_text_draw(self, text_list: list, text_rectangles: list, padding_y: int = 100):
        """Отрисовка текста в главном меню"""
        y = 50
        if len(text_list) > 1:
            y = 100
        for i in range(len(text_list)):
            y += padding_y * 1.2
            self.__screen.blit(text_list[i], (200 - (text_rectangles[i][2] / 2), y))
            pygame.display.update()

    @staticmethod
    def main_menu_text(text_list: list):
        """Создание коллайдеров для текста"""
        text_rectangles = []
        y = 50
        if len(text_list) > 1:
            y = 100
        for i in range(len(text_list)):
            y += 100 * 1.2
            text_rectangles.append(text_list[i].get_rect(left=100, top=y, width=300, height=100))
        return text_rectangles

    def show_win_message(self):
        """Показ сообщения о победе"""
        if self.__sound_result_not_played:
            self.__sound_result_not_played = False
            self.__level_was_not_entered = True
            self.__mixer.play_win_sound(0.3)
            # self.__is_game_continue = False
            print('winner')

    def show_game_over_message(self):
        """Показ сообщения о поражении"""
        if self.__sound_result_not_played:
            self.__sound_result_not_played = False
            self.__mixer.play_lose_sound(0.3)
            self.__is_game_continue = False
            print('failed')
            # self.show_level_game_over_menu()

    def show_how_much_mines_left(self):
        """Показ сообщения о количестве оставшихся бомб"""
        print(self.__model.how_much_mines_left(), 'мин осталось')
