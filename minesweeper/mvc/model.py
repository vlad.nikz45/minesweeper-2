import random

MIN_ROW_COUNT = 3
MAX_ROW_COUNT = 100

MIN_COLUMN_COUNT = 3
MAX_COLUMN_COUNT = 100

MIN_MINE_COUNT = 1
MAX_MINE_COUNT = 2500


class MinesweeperCell:
    """Ячейка в сапёре"""

    # Возможные состояния игровой клетки:
    #   closed - закрыта
    #   opened - открыта
    #   flagged - помечена флажком

    def __init__(self, row: int, column: int):
        """Инициализация"""
        self.__row = row
        self.__column = column
        self.__state = 'closed'
        self.__was_not_opened = True
        self.__was_not_flagged = True
        self.__mined = False
        self.__boom_not_created = True
        self.__counter = 0
        self.__have_heart = False
        self.__have_coin = False
        self.__coin_not_created = True
        self.__heart_not_created = True
        self.__have_inventory_item = False
        self.__id_inventory_item = None

    mark_sequence = ['closed', 'flagged']
    
    def next_mark(self):
        """Установка следующего состояния ячейки"""
        if self.state in self.mark_sequence:
            state_index = self.mark_sequence.index(self.state)
            self.state = self.mark_sequence[(state_index + 1) % len(self.mark_sequence)]

    def open(self):
        """Открытие ячейки. Установка состояния"""
        if self.state != 'flagged':
            self.state = 'opened'

    def do_flag(self):
        """Установка флага на закрытую ячейку"""
        if self.state == 'closed':
            self.state = 'flagged'

    @property
    def row(self):
        """Получение индекса строки"""
        return self.__row

    @property
    def column(self):
        """Получение индекса столбца"""
        return self.__column

    @property
    def was_not_opened(self):
        """Получение атрибута состояния ячейки, что она не была открыта"""
        return self.__was_not_opened

    @was_not_opened.setter
    def was_not_opened(self, value):
        """Установка атрибута состояния ячейки, что она не была открыта"""
        self.__was_not_opened = value

    @property
    def was_not_flagged(self):
        """Получение атрибута состояния ячейки, что она не была зафлажированна"""
        return self.__was_not_flagged

    @was_not_flagged.setter
    def was_not_flagged(self, value):
        """Установка атрибута состояния ячейки, что она не была зафлажированна"""
        self.__was_not_flagged = value

    @property
    def boom_not_created(self):
        """Получение атрибута состояния ячейки, что на ней не был произведён взрыв"""
        return self.__boom_not_created

    @boom_not_created.setter
    def boom_not_created(self, value):
        """Установка атрибута состояния ячейки, что на ней не был произведён взрыв"""
        self.__boom_not_created = value

    @property
    def coin_not_created(self):
        """Получение атрибута состояния ячейки, что на ней не был произведена анимация запуска монеты"""
        return self.__coin_not_created

    @coin_not_created.setter
    def coin_not_created(self, value):
        """Установка атрибута состояния ячейки, что на ней не был произведена анимация запуска монеты"""
        self.__coin_not_created = value

    @property
    def heart_not_created(self):
        """Получение атрибута состояния ячейки, что на ней не был произведена анимация запуска сердца"""
        return self.__heart_not_created

    @heart_not_created.setter
    def heart_not_created(self, value):
        """Установка атрибута состояния ячейки, что на ней не был произведена анимация запуска сердца"""
        self.__heart_not_created = value

    @property
    def state(self):
        """Получение состояния ячейки"""
        return self.__state

    @state.setter
    def state(self, value):
        """Установка состояния ячейки"""
        self.__state = value

    @property
    def mined(self):
        """Получение атрибута есть ли мина в ячейке"""
        return self.__mined

    @mined.setter
    def mined(self, value):
        """Установка мины в ячейку"""
        self.__mined = value

    @property
    def counter(self):
        """Получение цифры количества бомб вокруг ячейки"""
        return self.__counter

    @counter.setter
    def counter(self, value):
        """Установка цифры количества бомб вокруг ячейки в ячейку"""
        self.__counter = value

    @property
    def have_coin(self):
        """Получение атрибута есть ли монета в ячейке"""
        return self.__have_coin

    @have_coin.setter
    def have_coin(self, value):
        """Установка монеты в ячейку"""
        self.__have_coin = value

    @property
    def have_heart(self):
        """Получение атрибута есть ли сердце в ячейке"""
        return self.__have_heart

    @have_heart.setter
    def have_heart(self, value):
        """Установка сердца в ячейку"""
        self.__have_heart = value

    @property
    def have_inventory_item(self):
        """Получение атрибута есть ли предмет для инвентаря в ячейке"""
        return self.__have_inventory_item

    @have_inventory_item.setter
    def have_inventory_item(self, value):
        """Установка атрибута есть ли предмет для инвентаря в ячейке"""
        self.__have_inventory_item = value

    @property
    def id_inventory_item(self):
        """Получение из ячейки id лежащего предмета"""
        return self.__id_inventory_item

    @id_inventory_item.setter
    def id_inventory_item(self, value):
        """Установка в ячейку id лежащего предмета в ней"""
        self.__id_inventory_item = value


class MinesweeperModel:
    """Модель сапёра"""

    def __init__(self, row_number: int = 10, column_number: int = 10, mine_number: int = 10,
                 mine_damage: tuple = (1, 1), hearts_drop_number: int = 0, coins_drop_number: int = 0):
        """Инициализация"""

        if row_number in range(MIN_ROW_COUNT, MAX_ROW_COUNT + 1):
            self.__row_number = row_number
        else:
            self.__row_number = 5

        if column_number in range(MIN_COLUMN_COUNT, MAX_COLUMN_COUNT + 1):
            self.__column_number = column_number
        else:
            self.__column_number = 5

        if mine_number < self.__row_number * self.__column_number:
            if mine_number in range(MIN_MINE_COUNT, MAX_MINE_COUNT + 1):
                self.__mine_number = mine_number
        else:
            self.__mine_number = self.__row_number * self.__column_number - 1

        self.__first_step = True
        # self.__game_over = False
        self.__cells_table = []
        self.__number_bombs_left = self.mine_number
        self.__old_number_opened_mines = 0
        # self.__current_hp = 0
        self.__mine_damage = mine_damage
        self.__hearts_drop_number = hearts_drop_number
        self.__coins_drop_number = coins_drop_number
        self.__random_id_item = random.randint(1, 5)

        self.start_game()

    @property
    def mine_damage(self):
        """Возврат урона мины"""
        return self.__mine_damage

    @property
    def hearts_drop_number(self):
        """Возврат количества сердец на уровне"""
        return self.__hearts_drop_number

    @property
    def coins_drop_number(self):
        """Возврат количества монет на уровне"""
        return self.__coins_drop_number

    @property
    def row_number(self):
        """Получить количество строк"""
        return self.__row_number

    @property
    def column_number(self):
        """Получить количество столбцов"""
        return self.__column_number

    @property
    def mine_number(self):
        """Получить количество мин"""
        return self.__mine_number

    @property
    def cells_table(self):
        """Получить ячейки таблицы"""
        return self.__cells_table

    def start_game(self):
        """Запуск игры"""
        for row in range(self.__row_number):
            cells_row = []
            for column in range(self.__column_number):
                cells_row.append(MinesweeperCell(row, column))
            self.cells_table.append(cells_row)

    def get_cell(self, row: int, column: int):
        """Возвращает значение из ячейки"""
        if row < 0 or column < 0 or self.__row_number <= row or self.__column_number <= column:
            return None
        return self.cells_table[row][column]

    def is_win(self):
        """Проверка победы"""
        for row in range(self.__row_number):
            for column in range(self.__column_number):
                cell = self.cells_table[row][column]
                if not cell.mined and (cell.state != 'opened' or cell.state == 'flagged'):
                    return False
        return True

    # def is_game_over(self):
    #     """Возвращает проверку на победу"""
    #     return self.__game_over

    def open_cell(self, row: int, column: int) -> int:
        """Открытие ячейки"""

        cell = self.get_cell(row, column)
        if not cell:
            return 0

        cell.open()

        if cell.mined:
            hits = self.count_hits_per_turn()
            return self.random_damage_take_per_hit(hits)

        if self.__first_step:
            self.__first_step = False
            self.generate_mines()
            self.generate_item(self.__hearts_drop_number)
            self.generate_item(self.__coins_drop_number)
            self.generate_inventory_item(self.__random_id_item)

        cell.counter = self.count_mines_around_cell(row, column)
        if cell.counter == 0:
            self.open_neighbours(row, column)

        hits = self.count_hits_per_turn()
        return self.random_damage_take_per_hit(hits)

    def open_neighbours(self, row: int, column: int):
        """Открытие соседних ячеек, просчёт полученных ударов"""
        damage_sum = 0
        neighbours = self.get_cell_neighbours(row, column)
        for n in neighbours:
            if n.state == 'closed':
                damage_sum += self.open_cell(n.row, n.column)
        return damage_sum

    def do_flag_neighbours(self, row: int, column: int):
        """Флагирование соседних ячеек"""
        neighbours = self.get_cell_neighbours(row, column)
        for n in neighbours:
            if n.state == 'closed':
                n.next_mark()

    def next_cell_mark(self, row: int, column: int):
        """Если ячейка существует, то выполнить следующее состояние"""
        cell = self.get_cell(row, column)
        if cell:
            cell.next_mark()

    def generate_mines(self):
        """Генерация мин"""
        for _ in range(self.__mine_number):
            while True:
                row = random.randint(0, self.__row_number - 1)
                column = random.randint(0, self.__column_number - 1)
                cell = self.get_cell(row, column)
                if not cell.state == 'opened' and not cell.mined and self.count_opened_around_cell(row, column) == 0:
                    cell.mined = True
                    break

    def remove_heart_at_cell(self, row, column):
        """Убрать сердечко по забору"""
        cell = self.get_cell(row, column)
        cell.have_heart = False

    def remove_coin_at_cell(self, row, column):
        """Убрать монету по забору"""
        cell = self.get_cell(row, column)
        cell.have_coin = False

    def remove_item_at_cell(self, row, column):
        """Убрать предмет по забору"""
        cell = self.get_cell(row, column)
        cell.have_inventory_item = False

    def random_damage_take_per_hit(self, hits):
        """Рандомизирует каждый полученной удар в урон диапазона бомбы"""
        damage = 0
        for hit in range(1, hits + 1):
            random_damage_number = random.randint(self.__mine_damage[0], self.__mine_damage[1])
            damage += random_damage_number
        return damage

    def generate_item(self, item):
        """Генерация предмета"""
        for _ in range(item):
            for _ in range(5):
                row = random.randint(0, self.__row_number - 1)
                column = random.randint(0, self.__column_number - 1)
                cell = self.get_cell(row, column)
                cell.counter = self.count_mines_around_cell(row, column)
                if not cell.state == 'opened' and not cell.mined and cell.counter == 0 and cell.have_heart is not True \
                        and cell.have_coin is not True and cell.have_inventory_item is not True:
                    if item == self.__coins_drop_number:
                        cell.have_coin = True
                        break
                    elif item == self.__hearts_drop_number:
                        cell.have_heart = True
                        break

    def generate_inventory_item(self, identifier):
        """Генерация подбираемого предмета в инвентарь по id"""
        for _ in range(1):
            for _ in range(5):
                row = random.randint(0, self.__row_number - 1)
                column = random.randint(0, self.__column_number - 1)
                cell = self.get_cell(row, column)
                cell.counter = self.count_mines_around_cell(row, column)
                if not cell.state == 'opened' and not cell.mined and cell.counter == 0 and cell.have_heart is not True \
                        and cell.have_coin is not True and cell.have_inventory_item is not True:
                    cell.have_inventory_item = True
                    cell.id_inventory_item = identifier
                    break

    def open_cell_by_cell_opening_staff(self):
        """Открытие трёх случайных ячеек без бомбы посохом"""
        for _ in range(3):
            for _ in range(5000):
                row = random.randint(0, self.__row_number - 1)
                column = random.randint(0, self.__column_number - 1)
                cell = self.get_cell(row, column)
                if not cell.state == 'opened' and not cell.mined:
                    self.open_cell(row, column)
                    break

    def how_much_mines_left(self):
        """Возвращает значение ссколько мин осталось"""
        return self.count_all_flagged_cells()

    def do_flag_board(self):
        """Флагирует все оставшиеся ячейки"""
        for row in range(self.__row_number):
            for column in range(self.__column_number):
                cell = self.cells_table[row][column]
                if cell.state == 'closed':
                    cell.state = 'flagged'

    def count_all_flagged_cells(self) -> int:
        """Считает количество расставленных флагов и открытых бомб"""
        self.__number_bombs_left = self.mine_number
        for row in range(self.__row_number):
            for column in range(self.__column_number):
                cell = self.cells_table[row][column]
                if cell.state == 'flagged' or cell.state == 'opened' and cell.mined:
                    self.__number_bombs_left -= 1
        return self.__number_bombs_left

    def open_all_bombs(self):
        """Открывает все ячейки с бомбами"""
        for row in range(self.__row_number):
            for column in range(self.__column_number):
                cell = self.cells_table[row][column]
                if cell.state == 'closed' and cell.mined:
                    self.open_cell(row, column)

    def count_mines_around_cell(self, row: int, column: int) -> int:
        """Подсчёт количества мин вокруг ячейки"""
        neighbours = self.get_cell_neighbours(row, column)
        return sum(1 for n in neighbours if n.mined)

    def count_flags_around_cell(self, row: int, column: int) -> int:
        """Подсчёт количества расставленных флагов вокруг ячейки"""
        neighbours = self.get_cell_neighbours(row, column)
        return sum(1 for n in neighbours if n.state == 'flagged')

    def count_opened_around_cell(self, row: int, column: int) -> int:
        """Подсчёт количества открытых ячеек вокруг ячейки"""
        neighbours = self.get_cell_neighbours(row, column)
        return sum(1 for n in neighbours if n.state == 'opened')

    def count_opened_and_mined_around_cell(self, row: int, column: int) -> int:
        """Подсчёт количества открытх мин вокруг ячейки"""
        neighbours = self.get_cell_neighbours(row, column)
        return sum(1 for n in neighbours if n.mined and n.state == 'opened')

    def count_hits_per_turn(self) -> int:
        """Подсчёт открытых заминированных ячеек / Высчитывает полученный урон за ход"""
        opened_and_mined_number = 0
        for row in range(self.__row_number):
            for column in range(self.__column_number):
                cell = self.cells_table[row][column]
                if cell.state == 'opened' and cell.mined:
                    opened_and_mined_number += 1
        hits = opened_and_mined_number - self.__old_number_opened_mines
        self.__old_number_opened_mines = opened_and_mined_number
        return hits

    def count_close_around_cell(self, row: int, column: int) -> int:
        """Подсчёт количества закрытых ячеек вокруг ячейки"""
        neighbours = self.get_cell_neighbours(row, column)
        return sum(1 for n in neighbours if n.state == 'closed' or n.state == 'flagged')

    def get_cell_neighbours(self, row: int, column: int) -> list:
        """Возвращает соседей ячейки"""
        neighbours = []
        for r in range(row - 1, row + 2):
            neighbours.append(self.get_cell(r, column - 1))
            if r != row:
                neighbours.append(self.get_cell(r, column))
            neighbours.append(self.get_cell(r, column + 1))

        return filter(lambda n: n is not None, neighbours)
