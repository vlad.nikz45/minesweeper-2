#
# En:

### Implemented in the project:
1. The system of saves (completely, you can quit the game and everything will be saved). To delete saves, go to the saves folder and delete the file save_player_0.json.
2. A variety of levels with their own visual styles, mood, music and weather. A variety of bombs and varying damage from them.
3. Levels can randomly spawn hearts to restore life points, randomly spawn items to help (fruit, money, magic brush).
4. Weather and wind.
5. Smooth music playback so that music does not abruptly end or start (mixer). Sounds and fonts.
6. Animation when an explosion occurs.
7. Animation of spinning coins, hearts on levels.
8. Right clicking on a number helps to put flags around it.
9. Left click on a number helps to open cells around it.
10. Fruit and berry items increase the number of life points.
11. A shop that sells items for money.
12. A Magic Brush item that opens 3 random cells without bombs
13. Death screen and replay the game from the beginning.
14. End of game screen with credits.
15. Moving the playfield for convenience, and scaling it.
16. Character animation in the bottom panel, it winks ;)
17. Character statistics, inventory in the bottom panel.
18. Level selection menu, showing statistics of each level.
19. Start menu.
20. Basic logic of the Minesweeper game.
21. The code is written on MVC template.

### Authors: students ISU 14222/14224 Vladislav Nikitin, Sergey Vdovin, Alexander Domenenko, Pavel Orlov, Daniil Botvenko, Sergey Suslikov
### Year: 2022


#
# Ru:

### Реализовано в проекте:
1. Система сохранений (полностью, вы можете выйти из игры и всё сохранится). Для удаления сохранений зайдите в папку saves и удалить файл  save_player_0.json.
2. Разнообразные уровни со своими визуальными стилями, настроением, музыкой и погодой. Разнообразным количеством бомб и варьируемым уроном от них.
3.  На уровнях возможен случайный спавн сердец для восстановления очков жизней, случайный спавн предметов для помощи (фрукты, деньги, магическая кисть).
4. Погода и ветер.
5. Плавное проигрывание музыки, чтобы резко не кончалась и не начиналась музыка (микшер). Звуки и шрифты.
6. Анимация при взрыве.
7. Анимация кручения монеток, сердец на уровнях.
8. Клик правой кнопкой мыши на цифру помогает поставить флаги вокруг.
9. Клик левой кнопкой мыши на цифру помогает открыть ячейки вокруг.
10. Предметы-фрукты и ягоды увеличивают количество очков жизней.
11. Магазин, в котором продаются предметы за деньги.
12. Предмет Волшебная кисть, которая открывает 3 случайные ячейки без бомб
13. Экран смерти и повтор игры с самого начала.
14. Экран конца игры с титрами.
15. Перемещение игрового поля для удобства, и его масштабирование.
16. Анимация персонажа в нижней панели, он подмигивает ;)
17. Статистика персонажа, инвентарь в нижней панели.
18. Меню выбора уровня, показ статистик каждого уровня.
19. Стартовое меню.
20. Базовая логика игры Сапёр.
21. Код написан на MVC шаблоне.

### Авторы: студенты ИГУ 14222/14224 Владислав Никитин, Сергей Вдовин, Александр Домненко, Павел Орлов, Даниил Ботвенко, Сергей Сусликов
### Год: 2022

#

# Demonstration / Демонстрация

![Start](images/1.png "Start")
![Shop](images/2.png "Shop")
![Level selection](images/3.png "Level selection")
![Level 1](images/4.png "Level 1")
![Move and scale level](images/5.png "Move and scale level")
![Open cell](images/6.png "Open cell")
![Flag](images/7.png "Flag")
![Get coin](images/8.png "Get coin")
![Winning](images/9.png "Winning")
![Second Level](images/10.png "Second Level")
![Open cell](images/11.png "Open cell")
![Winning](images/12.png "Winning")
![Level 3](images/13.png "Level 3")
![Get hurted](images/14.png "Get hurted")
![Take heart](images/15.png "Take heart")
![Level 4](images/16.png "Level 4")
![Open level 4](images/17.png "Open level 4")
![Open cell](images/18.png "Open cell")
![Next level](images/19.png "Next level")
![Used items](images/20.png "Used items")
![Next level](images/21.png "Next level")
![Winning](images/22.png "Winning")
![Next level](images/23.png "Next level")
![Winning](images/24.png "Winning")
![Winning](images/25.png "Winning")
![Shop](images/26.png "Shop")
![Bought items](images/27.png "Bought items")
![Used items](images/28.png "Used items")
![Final level](images/29.png "Final level")
![Used magic brush](images/30.png "Used magic brush")
![Almost win](images/31.png "Almost win")
![Final banner](images/32.png "Final banner")
![Dead banner](images/33.png "Dead banner")

#
